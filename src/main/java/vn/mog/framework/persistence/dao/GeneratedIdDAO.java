package vn.mog.framework.persistence.dao;

import vn.mog.framework.persistence.model.GeneratedIdEntry;

public interface GeneratedIdDAO<T extends GeneratedIdEntry> extends UpdatableDAO<T, Long> {
	public T newInstance();
}
