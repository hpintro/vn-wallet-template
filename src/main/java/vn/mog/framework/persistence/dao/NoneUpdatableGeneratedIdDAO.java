package vn.mog.framework.persistence.dao;

import vn.mog.framework.persistence.model.NoneUpdatableGeneratedIdEntry;

public interface NoneUpdatableGeneratedIdDAO<T extends NoneUpdatableGeneratedIdEntry> extends BaseDAO<T, Long> {
	public T newInstance();
}
