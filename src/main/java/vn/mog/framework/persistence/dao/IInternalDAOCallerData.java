package vn.mog.framework.persistence.dao;

public interface IInternalDAOCallerData {
	public long getCallerId();
}
