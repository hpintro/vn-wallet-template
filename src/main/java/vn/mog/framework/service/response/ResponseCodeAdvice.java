package vn.mog.framework.service.response;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Map;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import vn.mog.framework.contract.base.KeyValue;
import vn.mog.framework.contract.base.MobiliserRequestType;
import vn.mog.framework.contract.base.MobiliserResponseType;
import vn.mog.framework.security.api.ICallerUtils;
import vn.mog.framework.service.api.IMobiliserExceptionTranslator;
import vn.mog.framework.service.api.MobiliserServiceException;

@Service
public final class ResponseCodeAdvice implements MethodInterceptor, InitializingBean {
	private static final Logger LOG = Logger.getLogger(ResponseCodeAdvice.class);

	private static final Class<?>[] EMPTY_CLASS_ARR = new Class[0];

	private static final Object[] EMPTY_OBJ_ARR = new Object[0];

	@Autowired
	private ICallerUtils callerUtils;
	@Autowired
	private IMobiliserExceptionTranslator translator;

	@Override
	public void afterPropertiesSet() {
		if (this.translator == null) {
			throw new IllegalStateException("translator is required");
		}

		if (this.callerUtils == null)
			throw new IllegalStateException("callerUtils is required");
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object[] arguments = invocation.getArguments();
		MobiliserRequestType request;
		if ((arguments != null) && (arguments.length == 1) && (arguments[0] instanceof MobiliserRequestType)) {
			request = (MobiliserRequestType) arguments[0];
		} else
			request = null;
		MobiliserResponseType newInstance;
		try {
			Object result = invocation.proceed();

			if (result instanceof MobiliserResponseType) {
				MobiliserResponseType mobResponse = (MobiliserResponseType) result;

				if (mobResponse.getStatus() == null) {
					MobiliserResponseType.Status status = new MobiliserResponseType.Status();
					status.setCode(0);

					mobResponse.setStatus(status);
				}

				if ((request != null) && (mobResponse.getConversationId() == null)) {
					mobResponse.setConversationId(request.getConversationId());
				}
			}

			return result;
		} catch (Throwable e) {
			Class returnType = invocation.getMethod().getReturnType();

			if (!(ClassUtils.isAssignable(MobiliserResponseType.class, returnType))) {
				throw e;
			}

			if (e instanceof MobiliserServiceException) {
				LOG.debug("Endpoint: [" + invocation.getThis().getClass().getSimpleName()
						+ "] threw an exception, attempting to " + "convert to status value for response type ["
						+ invocation.getMethod().getReturnType().getSimpleName() + "]", e);
			} else {
				LOG.warn("Endpoint: [" + invocation.getThis().getClass().getSimpleName()
						+ "] threw an exception, attempting to " + "convert to status value for response type ["
						+ invocation.getMethod().getReturnType().getSimpleName() + "]", e);
			}

			IMobiliserExceptionTranslator.TranslationResult translation = this.translator.translate(e,
					Long.valueOf(this.callerUtils.getCallerId()));

			if (translation == null) {
				if (LOG.isTraceEnabled()) {
					LOG.trace("Exception: " + e.getClass() + " should not be mapped.");
				}

				throw e;
			}

			MobiliserResponseType.Status status = new MobiliserResponseType.Status();
			status.setCode(translation.returnCode);
			status.setValue(translation.message);

			newInstance = instantiateResponse(returnType, e);

			newInstance.setStatus(status);

			if (request != null) {
				newInstance.setConversationId(request.getConversationId());
			}

			if (e instanceof MobiliserServiceException) {
				MobiliserServiceException mobServException = (MobiliserServiceException) e;

				Map<String, String> parameters = mobServException.getParameters();

				for (Map.Entry<String, String> entry : parameters.entrySet()) {
					KeyValue keyVal = new KeyValue();
					keyVal.setKey(entry.getKey());
					keyVal.setValue(entry.getValue());

					newInstance.getUnstructuredData().add(keyVal);
				}

			}

			LOG.info("Mapped exception to Status: " + newInstance.getStatus().getCode() + "/"
					+ newInstance.getStatus().getValue());
		}

		return newInstance;
	}

	@SuppressWarnings("rawtypes")
	private Constructor<?> findConstructor(Class<?> clazz, Class<?>[] paramTypes) {
		Assert.notNull(clazz, "Class must not be null");
		Assert.notNull(paramTypes, "paramTypes must not be null");

		Class searchType = clazz;

		while (searchType != null) {
			Constructor[] constructors = searchType.getDeclaredConstructors();

			for (Constructor constructor : constructors) {
				if (Arrays.equals(paramTypes, constructor.getParameterTypes())) {
					return constructor;
				}
			}
			searchType = searchType.getSuperclass();
		}

		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private MobiliserResponseType instantiateResponse(Class<?> returnType, Throwable e) throws Throwable {
		Constructor constructor;
		try {
			constructor = findConstructor(returnType, EMPTY_CLASS_ARR);
		} catch (Throwable e2) {
			LOG.warn("Unexpected exception while processing endpoint return type: " + returnType, e2);

			throw e;
		}

		if (constructor == null) {
			LOG.warn("Could not find suitable constructor for type: " + returnType);

			throw e;
		}
		MobiliserResponseType newInstance;
		try {
			newInstance = (MobiliserResponseType) BeanUtils.instantiateClass(constructor, EMPTY_OBJ_ARR);
		} catch (RuntimeException e2) {
			LOG.warn("Failed to instantiate instance of: " + returnType, e2);
			throw e;
		}

		return newInstance;
	}

	public void setCallerUtils(ICallerUtils callerUtils) {
		this.callerUtils = callerUtils;
	}

	public void setTranslator(IMobiliserExceptionTranslator translator) {
		this.translator = translator;
	}
}
