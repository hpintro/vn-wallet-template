package vn.mog.framework.workflow;

import java.util.List;

import vn.mog.framework.workflow.exception.WorkflowException;

public interface Processor {

	public boolean supports(Activity<? extends ProcessContext<?>> activity);

	public ProcessContext<?> doActivities() throws WorkflowException;

	public ProcessContext<?> doActivities(Object seedData) throws WorkflowException;

	public void setActivities(List<Activity<ProcessContext<?>>> activities);

	public void setDefaultErrorHandler(ErrorHandler defaultErrorHandler);

	public void setProcessContextFactory(ProcessContextFactory<Object, Object> processContextFactory);

}
