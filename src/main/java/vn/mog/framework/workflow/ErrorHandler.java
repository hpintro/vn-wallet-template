package vn.mog.framework.workflow;

import org.springframework.beans.factory.BeanNameAware;

import vn.mog.framework.workflow.exception.WorkflowException;

public interface ErrorHandler extends BeanNameAware {

	public void handleError(ProcessContext<?> context, Throwable th) throws WorkflowException;

}
