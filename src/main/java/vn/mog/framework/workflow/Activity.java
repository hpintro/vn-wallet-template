package vn.mog.framework.workflow;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

public interface Activity<T extends ProcessContext<?>> extends BeanNameAware, Ordered {
	/**
	 * Called by the encompassing processor to activate the execution of the
	 * Activity
	 * 
	 * @param context
	 *            - process context for this workflow
	 * @return resulting process context
	 * @throws Exception
	 */
	public T execute(T context) throws Exception;

	/**
	 * Determines if an activity should execute based on the current values in
	 * the {@link ProcessContext}. For example, a context might have both an
	 * {@link Order} as well as a String 'status' of what the order should be
	 * changed to. It is possible that an activity in a workflow could only deal
	 * with a particular status change, and thus could return false from this
	 * method.
	 * 
	 * @param context
	 * @return
	 */
	public boolean shouldExecute(T context);

	/**
	 * Get the fine-grained error handler wired up for this Activity
	 * 
	 * @return
	 */
	public ErrorHandler getErrorHandler();

	public void setErrorHandler(ErrorHandler errorHandler);

	public String getBeanName();
}
