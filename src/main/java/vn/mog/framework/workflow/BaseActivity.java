package vn.mog.framework.workflow;

import org.springframework.core.Ordered;

public abstract class BaseActivity<T extends ProcessContext<?>> implements Activity<T> {

	protected ErrorHandler errorHandler;
	protected String beanName;

	protected int order = Ordered.LOWEST_PRECEDENCE;

	@Override
	public boolean shouldExecute(T context) {
		return true;
	}

	@Override
	public ErrorHandler getErrorHandler() {
		return errorHandler;
	}

	@Override
	public void setErrorHandler(final ErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	@Override
	public String getBeanName() {
		return beanName;
	}

	@Override
	public void setBeanName(final String beanName) {
		this.beanName = beanName;
	}

	@Override
	public int getOrder() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setOrder(int order) {
		this.order = order;
	}
}
