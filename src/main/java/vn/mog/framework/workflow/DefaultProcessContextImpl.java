package vn.mog.framework.workflow;

public class DefaultProcessContextImpl<T> implements ProcessContext<T> {
	public final static long serialVersionUID = 1L;
	protected T seedData;
	protected boolean stopEntireProcess = false;

	public boolean stopProcess() {
		this.stopEntireProcess = true;
		return stopEntireProcess;
	}

	public boolean isStopped() {
		return stopEntireProcess;
	}

	public T getSeedData() {
		return seedData;
	}

	public void setSeedData(T seedObject) {
		seedData = (T) seedObject;
	}

}
