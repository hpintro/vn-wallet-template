package vn.mog.framework.workflow.exception;

public abstract class BaseException extends Exception implements RootCauseAccessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Throwable rootCause;

	public BaseException() {
		super();
	}

	public BaseException(String message, Throwable cause) {
		super(message, cause);
		if (cause != null) {
			rootCause = findRootCause(cause);
		} else {
			rootCause = this;
		}
	}

	private Throwable findRootCause(Throwable cause) {
		Throwable rootCause = cause;
		while (rootCause != null && rootCause.getCause() != null) {
			rootCause = rootCause.getCause();
		}
		return rootCause;
	}

	public BaseException(String message) {
		super(message);
		this.rootCause = this;

	}

	public BaseException(Throwable cause) {
		super(cause);
		if (cause != null) {
			rootCause = findRootCause(cause);
		}
	}

	public Throwable getRootCause() {
		return rootCause;
	}

	public String getRootCauseMessage() {
		if (rootCause != null) {
			return rootCause.getMessage();
		} else {
			return getMessage();
		}
	}

}
