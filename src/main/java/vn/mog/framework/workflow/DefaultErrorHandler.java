package vn.mog.framework.workflow;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import vn.mog.framework.workflow.exception.WorkflowException;

public class DefaultErrorHandler implements ErrorHandler {

	private static final Logger logger = Logger.getLogger(DefaultErrorHandler.class);
	@SuppressWarnings("unused")
	private String name;

	protected List<String> unloggedExceptionClasses = new ArrayList<String>();

	@Override
	public void handleError(ProcessContext<?> context, Throwable th) throws WorkflowException {
		context.stopProcess();

		boolean shouldLog = true;
		Throwable cause = th;
		while (true) {
			if (unloggedExceptionClasses.contains(cause.getClass().getName())) {
				shouldLog = false;
				break;
			}
			cause = cause.getCause();
			if (cause == null) {
				break;
			}
		}
		if (shouldLog) {
			logger.error("An error occurred during the workflow", th);
		}

		throw new WorkflowException(th);
	}

	@Override
	public void setBeanName(String name) {
		this.name = name;
	}

	public List<String> getUnloggedExceptionClasses() {
		return unloggedExceptionClasses;
	}

	public void setUnloggedExceptionClasses(List<String> unloggedExceptionClasses) {
		this.unloggedExceptionClasses = unloggedExceptionClasses;
	}

}
