package vn.mog.framework.workflow;

import vn.mog.framework.workflow.exception.WorkflowException;

public interface ProcessContextFactory<U, T> {

	public ProcessContext<U> createContext(T preSeedData) throws WorkflowException;

}
