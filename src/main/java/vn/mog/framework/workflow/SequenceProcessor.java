package vn.mog.framework.workflow;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import vn.mog.framework.workflow.exception.WorkflowException;

public class SequenceProcessor extends BaseProcessor {

	private static final Log logger = LogFactory.getLog(SequenceProcessor.class);

	private ProcessContextFactory<Object, Object> processContextFactory;

	@Override
	public boolean supports(Activity<? extends ProcessContext<?>> activity) {
		// TODO Auto-generated method stub
		return (activity instanceof BaseActivity);
	}

	@Override
	public ProcessContext<?> doActivities() throws WorkflowException {
		// TODO Auto-generated method stub
		return doActivities(null);
	}

	@Override
	public ProcessContext<?> doActivities(Object seedData) throws WorkflowException {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) {
			logger.debug(getBeanName() + " processor is running..");
		}

		// retrieve injected by Spring

		List<Activity<ProcessContext<? extends Object>>> activities = getActivities();

		// retrieve a new instance of the Workflow ProcessContext
		ProcessContext<?> context = createContext(seedData);

		for (Iterator<Activity<ProcessContext<?>>> it = activities.iterator(); it.hasNext();) {
			Activity<ProcessContext<?>> activity = it.next();
			if (activity.shouldExecute(context)) {
				if (logger.isDebugEnabled())
					logger.debug("running activity:" + activity.getBeanName() + " using arguments:" + context);

				try {
					context = activity.execute(context);
				} catch (Throwable th) {
					try {
						if (getAutoRollbackOnError()) {
							logger.info("Automatically rolling back state for any previously registered "
									+ "RollbackHandlers. RollbackHandlers may be registered for workflow activities"
									+ " in appContext.");
							// ActivityStateManagerImpl.getStateManager().rollbackAllState();
						}
						ErrorHandler errorHandler = activity.getErrorHandler();
						if (errorHandler == null) {
							logger.info("no error handler for this action, run default error"
									+ "handler and abort processing ");
							getDefaultErrorHandler().handleError(context, th);
							break;
						} else {
							logger.info("run error handler and continue");
							errorHandler.handleError(context, th);
						}
					} catch (RuntimeException e) {
						logger.error(
								"An exception was caught while attempting to handle an activity generated exception",
								e);
						throw e;
					} catch (WorkflowException e) {
						logger.error(
								"An exception was caught while attempting to handle an activity generated exception",
								e);
						throw e;
					}
				}

				// ensure its ok to continue the process
				if (processShouldStop(context, activity))
					break;

			} else {
				logger.debug("Not executing activity: " + activity.getBeanName() + " based on the context: " + context);
			}
		}
		logger.debug(getBeanName() + " processor is done.");
		return context;
	}

	/**
	 * Determine if the process should stop
	 * 
	 * @param context
	 *            the current process context
	 * @param activity
	 *            the current activity in the iteration
	 */
	protected boolean processShouldStop(ProcessContext<?> context, Activity<? extends ProcessContext<?>> activity) {
		if (context == null || context.isStopped()) {
			logger.info("Interrupted workflow as requested by:" + activity.getBeanName());
			return true;
		}
		return false;
	}

	protected ProcessContext<Object> createContext(Object seedData) throws WorkflowException {
		return processContextFactory.createContext(seedData);
	}

	@Override
	public void setProcessContextFactory(ProcessContextFactory<Object, Object> processContextFactory) {
		this.processContextFactory = processContextFactory;
	}

}
