package vn.mog.util.messaging.service.email;

import java.util.List;

public interface IMailSender {
	public boolean send(String subject, String body, List<byte[]> attachement, String[] fileName, String[] contentType,
			String from, String[] to, String[] cc, String[] bcc, String[] replyTo);
}
