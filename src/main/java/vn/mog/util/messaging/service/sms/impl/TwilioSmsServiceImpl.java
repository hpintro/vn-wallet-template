package vn.mog.util.messaging.service.sms.impl;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.truemoney.ewallet.common.util.Utils;
import com.truemoney.ewallet.common.util.VnUtils;

import vn.mog.util.messaging.service.sms.SmsService;

@Service("twilioSmsService")
public class TwilioSmsServiceImpl implements SmsService {
	private static final Logger logger = Logger.getLogger(TwilioSmsServiceImpl.class);

	@Override
	public boolean sendSMSMessage(String msisdn, String content) {
		// TODO Auto-generated method stub
		try {
			msisdn = Utils.getFormatedMsisdn(msisdn);
			System.out.println("/*----SEND SMS BRAND BEGIN----*/");
			System.out.println("Receivers: " + msisdn);
			System.out.println("Content: " + VnUtils.removeTone(content));
			System.out.println("/*----SEND SMS BRAND END----*/");

			JSONObject request = new JSONObject();
			request.put("phone", msisdn);
			request.put("text", VnUtils.removeTone(content));
			System.out.println(request.toString());

			HttpClient httpClient = new HttpClient();
			httpClient.getParams().setParameter("http.protocol.content-charset", "UTF-8");
			StringRequestEntity requestEntity = new StringRequestEntity(request.toString(), "application/json",
					"UTF-8");

			PostMethod postMethod = new PostMethod("https://api.xxx.com/sns/v1/sms");
			postMethod.addRequestHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0");
			postMethod.addRequestHeader("Content-Type", "application/json");
			postMethod.addRequestHeader("Authorization", "Token xxx");
			postMethod.setRequestEntity(requestEntity);

			int statusCode = httpClient.executeMethod(postMethod);
			System.out.println(statusCode);
			String response = postMethod.getResponseBodyAsString();
			System.out.println(response);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		return false;
	}

	public static void main(String[] args) throws Exception {
		SmsService smsService = new TwilioSmsServiceImpl();
		smsService.sendSMSMessage("84936663369", "TEST");
	}
}
