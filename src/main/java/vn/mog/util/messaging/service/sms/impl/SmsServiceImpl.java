package vn.mog.util.messaging.service.sms.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.truemoney.ewallet.SharedConstants;
import com.truemoney.ewallet.common.util.Utils;
import com.truemoney.ewallet.common.util.VnUtils;

import vn.mog.util.messaging.service.sms.SmsService;
import vn.mog.util.messaging.service.sms.bean.StatusSms;

@Service("defaultSmsService")
public class SmsServiceImpl implements SmsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SmsServiceImpl.class);

	// --------------------------------
	public static RestTemplate restTemplate = null;
	static {
		restTemplate = new RestTemplate();
		ObjectMapper lax = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
		jacksonConverter.setObjectMapper(lax);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(jacksonConverter);

		restTemplate.setMessageConverters(messageConverters);
	}

	// SEND SMS --------------------------------
	private static <T> T sendSMSMessage(String brand, String msisdn, String content, Class<T> clazz) {
		try {

			msisdn = Utils.getFormatedMsisdn(msisdn);
			System.out.println("/*----SEND SMS BRAND BEGIN----*/");
			System.out.println("Receivers: " + msisdn);
			System.out.println("Content: " + VnUtils.removeTone(content));
			System.out.println("/*----SEND SMS BRAND END----*/");

			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(SharedConstants.SMS_API_URL)
					.queryParam("username", SharedConstants.SMS_API_USER_NAME)
					.queryParam("password", SharedConstants.SMS_API_PASSWORD).queryParam("brand", brand)
					.queryParam("receiverNumber", msisdn).queryParam("message", content);

			HttpEntity<?> entity = new HttpEntity<>(headers);

			HttpEntity<T> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity,
					clazz);
			return response.getBody();
		} catch (HttpStatusCodeException ex) {
			int statusCode = ex.getStatusCode().value();
			LOGGER.info("RESPONE HTTP CODE: " + statusCode);
		} catch (RestClientException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public boolean sendSMSMessage(String msisdn, String content) {
		try {
			StatusSms resp = (StatusSms) SmsServiceImpl.sendSMSMessage(SharedConstants.SMS_API_BRAND, msisdn, content,
					StatusSms.class);
			if (resp != null && resp.getStatus() == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
