package vn.mog.util.messaging.service.sms;

public interface SmsService {

	String BEAN_NAME = "defaultSmsService";
	// String BEAN_NAME = "twilioSmsService";

	public boolean sendSMSMessage(String msisdn, String content);
}
