package vn.mog.util.messaging.service.email.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import vn.mog.util.messaging.service.email.IMailSender;

public class GmailSender implements IMailSender {
	static final Logger logger = Logger.getLogger(GmailSender.class);

	private String mailTitle = "1Pay Topup System";

	private String smtpHost = "smtp.gmail.com";
	private int smtpPort = 465; // 587; //465
	private String smtpAuthUser = "1paytopup@gmail.com";
	private String smtpAuthPwd = "";

	public void setMailTitle(String mailTitle) {
		this.mailTitle = mailTitle;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public void setSmtpAuthUser(String smtpAuthUser) {
		this.smtpAuthUser = smtpAuthUser;
	}

	public void setSmtpAuthPwd(String smtpAuthPwd) {
		this.smtpAuthPwd = smtpAuthPwd;
	}

	@Override
	public boolean send(String subject, String body, List<byte[]> attachement, String[] fileName, String[] contentType,
			String from, String[] to, String[] cc, String[] bcc, String[] replyTo) {

		try {
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", smtpHost);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");

			if (smtpPort == 465) {
				props.put("mail.smtp.socketFactory.port", smtpPort);
				// props.put("mail.smtp.socketFactory.class",
				// "javax.net.ssl.SSLSocketFactory");
			} else
				props.put("mail.smtp.port", smtpPort);

			Authenticator auth = new SMTPAuthenticator(smtpAuthUser, smtpAuthPwd);
			Session mailSession = Session.getInstance(props, auth);

			// uncomment for debugging infos to stdout
			mailSession.setDebug(true);
			Transport transport = mailSession.getTransport();

			MimeMessage message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(from, mailTitle));
			if (to != null)
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(arrayToString(to)));
			if (cc != null)
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(arrayToString(cc)));
			if (bcc != null)
				message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(arrayToString(bcc)));
			if (replyTo != null)
				message.setReplyTo(parseToAddresses(replyTo));

			message.setSubject(subject, "UTF-8");
			message.setText(body);

			// Add a MIME part to the message
			MimeMultipart mimeBodyPart = new MimeMultipart();
			BodyPart part = new MimeBodyPart();
			part.setContent(body, "text/html; charset=utf-8");
			mimeBodyPart.addBodyPart(part);

			// Add a attachement to the message
			if (attachement != null && fileName != null && contentType != null && attachement.size() == fileName.length
					&& attachement.size() == contentType.length) {
				for (int i = 0; i < attachement.size(); i++) {
					try {
						part = new MimeBodyPart();
						DataSource source = new ByteArrayDataSource(attachement.get(i), contentType[i]);
						part.setDataHandler(new DataHandler(source));
						part.setFileName(fileName[i]);
						mimeBodyPart.addBodyPart(part);
					} catch (Exception e) {
					}
				}
			}
			message.setContent(mimeBodyPart);

			transport.connect();
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

			return true;
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return false;
	}

	private String arrayToString(String[] arr) {
		if (arr == null || arr.length == 0)
			return null;

		return Arrays.asList(arr).toString().replace("[", "").replace("]", "");
	}

	private Address parseToAddress(String address) throws AddressException {
		if (StringUtils.isBlank(address))
			return null;

		return new InternetAddress(address);
	}

	private Address[] parseToAddresses(String[] addresses) throws AddressException {
		if (addresses == null || addresses.length == 0)
			return null;

		List<Address> addressesList = new ArrayList<Address>();
		for (String address : addresses)
			addressesList.add(parseToAddress(address));

		return addressesList.toArray(new Address[0]);
	}

	private class SMTPAuthenticator extends javax.mail.Authenticator {

		private String username;
		private String password;

		public SMTPAuthenticator(String username, String password) {
			this.username = username;
			this.password = password;
		}

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}

	public static void main(String[] args) throws Exception {
		String from = "hoangphuong@1pay.vn";
		String[] to = { "hpintro@gmail.com" };
		String[] cc = null; // { "ha@1pay.vn", "hahm@1pay.vn" };
		String[] bcc = null; // { "ha@1pay.vn", "hahm@1pay.vn" };
		String[] replyTo = { "hoangphuong@1pay.vn" };// { "ha@1pay.vn",
		// "hahm@1pay.vn" };
		String subject = from + " Onepay Topup";
		String message = "<h2>HọcHọc.com gửi bạn 314 Bài Học Tiếng Anh Bằng Hình </h2>";

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				GmailSender.class.getResourceAsStream("/Email_Template_System_Warning_Provider.html"), "UTF-8"));
		StringBuffer stringBuffer = new StringBuffer();
		String line = null;
		while ((line = reader.readLine()) != null) {
			stringBuffer.append(line);
		}
		message = stringBuffer.toString();

		/*
		 * List<byte[]> attachement = new ArrayList<byte[]>(); File[]
		 * attachements = { new
		 * File("/media/hp/Workspace/1pay/download2/MOG_SM15_21_09_2015.xls"),
		 * new
		 * File("/media/hp/Workspace/1pay/download2/MOG_SM15_21_09_2015.xls"),
		 * new
		 * File("/media/hp/Workspace/1pay/download2/MOG_SM15_21_09_2015.xls") };
		 * for (File f : attachements)
		 * attachement.add(org.apache.commons.io.IOUtils.toByteArray(new
		 * FileInputStream(f)));
		 * 
		 * String[] fileName = { "xxx1.xls", "xxx2.xls", "xxx3.xls" }; String[]
		 * contentType = { "application/vnd.ms-excel",
		 * "application/vnd.ms-excel", "application/vnd.ms-excel" };
		 */

		List<byte[]> attachement = null;
		String[] fileName = null;
		String[] contentType = null;
		GmailSender sendMail = new GmailSender();
		sendMail.send(subject, message, attachement, fileName, contentType, from, to, cc, bcc, replyTo);
	}
}
