package com.truemoney.ewallet.persistence.model.customer;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import vn.mog.framework.persistence.model.GeneratedIdEntry;

@Entity
@Table(name = "user_present")
@AttributeOverride(name = "id", column = @Column(name = "ID"))
public class UserPresent extends GeneratedIdEntry implements Serializable{
	@Column(name = "receiver")
	private String receiver;

	@Column(name = "present_id")
	private Long presentId;

	public UserPresent() {
	}
	public UserPresent(Long presentId) {
		this.presentId = presentId;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public Long getPresentId() {
		return presentId;
	}

	public void setPresentId(Long presentId) {
		this.presentId = presentId;
	}

	
}
