package com.truemoney.ewallet.persistence.model.customer;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import vn.mog.framework.persistence.model.GeneratedIdEntry;

@SuppressWarnings("serial")
@Entity
@Table(name = "BIZ_CUSTOMER")
@AttributeOverride(name = "id", column = @Column(name = "ID"))
public class Customer extends GeneratedIdEntry implements Serializable {
	@Column(name = "CIF", unique = true)
	private String cif;

	@Column(name = "USERNAME", unique = true)
	private String username;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "MSISDN")
	private String msisdn;

	@Column(name = "FULL_NAME")
	private String fullName;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "SECRET")
	private String secret;

	@Column(name = "AUTH_URL")
	private String authURL;

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getAuthURL() {
		// TODO Auto-generated method stub
		return this.authURL;
	}

	public void setAuthURL(String authURL) {
		this.authURL = authURL;
	}

}
