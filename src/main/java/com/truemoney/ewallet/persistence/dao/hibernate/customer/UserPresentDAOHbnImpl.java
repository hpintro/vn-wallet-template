package com.truemoney.ewallet.persistence.dao.hibernate.customer;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.truemoney.ewallet.persistence.dao.api.customer.UserPresentDAO;
import com.truemoney.ewallet.persistence.model.customer.UserPresent;

import vn.mog.framework.persistence.hinernate.dao.GeneratedIdDAOHbnImpl;

@Repository
public class UserPresentDAOHbnImpl extends GeneratedIdDAOHbnImpl<UserPresent> implements UserPresentDAO {
	@Override
	public Class<UserPresent> getEntityClass() {
		return UserPresent.class;
	}

	@Override
	public UserPresent getByPresentId(Long id) {
		return getByFieldName("presentId", id);
	}

	public UserPresent getByFieldName(String fieldName, Object fieldValue) {
		Criteria criteria = getSession().createCriteria(getEntityClass());
		criteria.add(Restrictions.eq(fieldName, fieldValue));
		criteria.add(Restrictions.isNull("receiver"));
		
		criteria.setMaxResults(1);
		criteria.setLockMode(LockMode.UPGRADE);
		
		List<UserPresent> pre = criteria.list();
		System.out.println(pre.size());
		if(pre != null &&  pre.size() > 0)
			return pre.get(0);
		return null;
	}
}
