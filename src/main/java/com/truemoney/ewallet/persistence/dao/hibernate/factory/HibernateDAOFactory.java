package com.truemoney.ewallet.persistence.dao.hibernate.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.truemoney.ewallet.persistence.dao.api.customer.CustomerDAO;
import com.truemoney.ewallet.persistence.dao.api.customer.UserPresentDAO;
import com.truemoney.ewallet.persistence.dao.api.factory.DaoFactory;

@Repository
public class HibernateDAOFactory implements DaoFactory {
	// -------- CUSTOMER ------//
	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private UserPresentDAO userPresentDAO;
	
	@Override
	public CustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	@Override
	public UserPresentDAO getUserPresentDAO() {
		return userPresentDAO;
	}
}
