package com.truemoney.ewallet.persistence.dao.api.customer;

import com.truemoney.ewallet.persistence.model.customer.UserPresent;

import vn.mog.framework.persistence.dao.GeneratedIdDAO;

public interface UserPresentDAO extends GeneratedIdDAO<UserPresent> {
	UserPresent getByPresentId(Long presentId);
}
