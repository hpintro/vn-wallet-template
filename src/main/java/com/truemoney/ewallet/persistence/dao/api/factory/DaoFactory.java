package com.truemoney.ewallet.persistence.dao.api.factory;

import com.truemoney.ewallet.persistence.dao.api.customer.CustomerDAO;
import com.truemoney.ewallet.persistence.dao.api.customer.UserPresentDAO;

public interface DaoFactory {
	// -------- CUSTOMER ------//
	CustomerDAO getCustomerDAO();
	
	UserPresentDAO getUserPresentDAO();

}
