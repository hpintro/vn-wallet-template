package com.truemoney.ewallet.persistence.dao.api.customer;

import com.truemoney.ewallet.persistence.model.customer.Customer;

import vn.mog.framework.persistence.dao.GeneratedIdDAO;

public interface CustomerDAO extends GeneratedIdDAO<Customer> {
	Customer getByCif(String cif);
}
