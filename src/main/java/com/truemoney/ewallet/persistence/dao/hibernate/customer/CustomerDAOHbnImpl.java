package com.truemoney.ewallet.persistence.dao.hibernate.customer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.truemoney.ewallet.persistence.dao.api.customer.CustomerDAO;
import com.truemoney.ewallet.persistence.model.customer.Customer;

import vn.mog.framework.persistence.hinernate.dao.GeneratedIdDAOHbnImpl;

@Repository
public class CustomerDAOHbnImpl extends GeneratedIdDAOHbnImpl<Customer> implements CustomerDAO {
	@Override
	public Class<Customer> getEntityClass() {
		return Customer.class;
	}

	@Override
	public Customer getByCif(String cif) {
		return getByFieldName("cif", cif);
	}

	public Customer getByFieldName(String fieldName, Object fieldValue) {
		Criteria criteria = getSession().createCriteria(getEntityClass());
		criteria.add(Restrictions.eq(fieldName, fieldValue));

		return (Customer) criteria.uniqueResult();
	}

}
