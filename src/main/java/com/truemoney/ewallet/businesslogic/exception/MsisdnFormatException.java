package com.truemoney.ewallet.businesslogic.exception;

public class MsisdnFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public MsisdnFormatException(String msg) {
		super(msg);
	}

	public MsisdnFormatException(Throwable th) {
		super(th);
	}

	public MsisdnFormatException(String msg, Throwable th) {
		super(msg, th);
	}
}
