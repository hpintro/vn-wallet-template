package com.truemoney.ewallet.businesslogic.exception;

public class NotPermittedException extends PaymentServiceException {

	private static final long serialVersionUID = 1L;

	public NotPermittedException(String message) {
		super(message);
	}

	public NotPermittedException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public NotPermittedException(int errorCode, String errorMessage, String source) {
		super(errorCode, errorMessage, source);
	}
}
