package com.truemoney.ewallet.businesslogic.exception;

public class NotAuthorizedException extends PaymentServiceException {

	private static final long serialVersionUID = 1L;

	public NotAuthorizedException(String message) {
		super(message);
	}

	public NotAuthorizedException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public NotAuthorizedException(int errorCode, String errorMessage, String source) {
		super(errorCode, errorMessage, source);
	}
}
