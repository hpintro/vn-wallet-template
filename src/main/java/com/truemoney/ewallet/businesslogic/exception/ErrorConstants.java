package com.truemoney.ewallet.businesslogic.exception;

public enum ErrorConstants {
	SUCCESS(0, "Success!"), ERROR(1, "Some exception has occurred!"), NOT_AUTHORIZED(2,
			"Unauthorized. Please sign in again!"), NOT_PERMISTTED(3, "Action is not permitted!"),

	CHANGE_STATUS_ERROR(4, "Error in changing status."),

	ENTITY_NOT_FOUND(5, "The entity not exist."),

	// Account
	INSUFFICIENT_BALANCE(800, "Insufficient balance!"), PAYMENT_INSTRUMENT_NOT_EXIST(801,
			"Payment instruments not exist!"),

	// Payment
	PAYMENT_FAIL(900, "Fail to pay!"), AUTHORISE_FAIL(901, "Authorise failure!"), AUTHORISATION_CANCEL_FAIL(902,
			"Authorisation cancel failure!"), CAPTURE_FAIL(903,
					"Capture failure!"), CAPTURE_CANCEL_FAIL(904, "Capture cancel failure!"),

	// ProviderConnection
	PROVIDER_CONNECTION_UNAVAILABLE(1000, "Provider connection unavailable!"), PROVIDER_TOPUP_CONNECTION_NOT_WORKING(
			1001, "Provider topup connection not working!"), PROVIDER_SOFTPIN_CONNECTION_NOT_WORKING(1002,
					"Provider download softpin connection not working!"), PROVIDER_NOT_UPDATE(1003,
							"Provider cant not update!"),

	PROVIDER_FUND_IN_CONNECTION_NOT_WORKING(1003, "Provider fund in connection not working!"),

	NO_SUPPORTING_PROVIDER(1004, "No supporting provider!"), ORDER_PROVIDER_BY_PROBABILITY_ERROR(1005,
			"Error in order provider by probability."),
	//
	BAD_REQUEST(2000, "Bad request!"), BAD_TOPUP_REQUEST(2001, "Topup bad request!"), BAD_CARD_REQUEST(2002,
			"Card bad request!"), BAD_PHONE_NUMBER(2003, "This number is not supported!"), SERVICE_NOT_DEFINED(2004,
					"Service not defined!"), TXN_DUPLICATED(2005, "Transaction is duplicated!"),

	// Account
	REGISTER_ACCOUNT_FAIL(2004, "Fail to register!"),

	// Fund
	PAYER_NOT_EXIST(2051, "The target account not exist!"), PAYEE_NOT_EXIST(2050,
			"The target account not exist!"), DEPOSIT_NOT_EXIST(2061,
					"The Deposit not exist!"), PARAM_MISSING(2060, "Param missing!");

	public String message;
	public int code;

	ErrorConstants(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public static ErrorConstants parse(int code) {
		ErrorConstants status = null; // Default
		for (ErrorConstants item : ErrorConstants.values()) {
			if (item.code == code) {
				status = item;
				break;
			}
		}
		return status;
	}
}
