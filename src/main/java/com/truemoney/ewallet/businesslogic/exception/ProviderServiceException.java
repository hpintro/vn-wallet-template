package com.truemoney.ewallet.businesslogic.exception;

public class ProviderServiceException extends PaymentServiceException {

	private static final long serialVersionUID = 1L;

	public ProviderServiceException(String msg) {
		super(msg);
	}

	public ProviderServiceException(Throwable th) {
		super(th);
	}

	public ProviderServiceException(String msg, Throwable th) {
		super(msg, th);
	}

	public ProviderServiceException(String code, String errorMessage) {
		super(code, errorMessage);
	}

	public ProviderServiceException(String code, String errorMessage, String source) {
		super(code, errorMessage, source);
	}

	public ProviderServiceException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public ProviderServiceException(int errorCode, String errorMessage, String source) {
		super(errorCode, errorMessage, source);
	}
}
