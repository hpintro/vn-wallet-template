package com.truemoney.ewallet.businesslogic.exception;

public class InternalServerException extends PaymentServiceException {

	private static final long serialVersionUID = 1L;

	public InternalServerException(String message) {
		super(message);
	}

	public InternalServerException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public InternalServerException(int errorCode, String errorMessage, String source) {
		super(errorCode, errorMessage, source);
	}
}
