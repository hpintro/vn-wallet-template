package com.truemoney.ewallet.businesslogic.exception;

public class PaymentServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private Integer errorCode;
	private String code;
	private String errorMessage;
	private String source;

	public PaymentServiceException() {
	}

	public PaymentServiceException(String code, String errorMessage) {
		this.setCode(code);
		this.setErrorMessage(errorMessage);
	}

	public PaymentServiceException(String code, String errorMessage, String source) {
		this.setCode(code);
		this.setErrorMessage(errorMessage);
		this.setSource(source);
	}

	public PaymentServiceException(Integer errorCode, String errorMessage) {
		this.setErrorCode(errorCode);
		this.setErrorMessage(errorMessage);
	}

	public PaymentServiceException(Integer errorCode, String errorMessage, String source) {
		this.setErrorCode(errorCode);
		this.setSource(source);
		this.setErrorMessage(errorMessage);
	}

	public String getNormalizeCode() {
		if (errorCode != null)
			return String.valueOf(errorCode);
		else
			return code;
	}

	public PaymentServiceException(String message) {
		super(message);
	}

	public PaymentServiceException(Throwable th) {
		super(th);
	}

	public PaymentServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorCode(Integer newVal) {
		errorCode = newVal;
	}

	public void setErrorMessage(String newVal) {
		errorMessage = newVal;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
