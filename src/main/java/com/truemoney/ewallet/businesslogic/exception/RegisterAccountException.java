package com.truemoney.ewallet.businesslogic.exception;

public class RegisterAccountException extends PaymentServiceException {

	private static final long serialVersionUID = 1L;

	public RegisterAccountException(String msg) {
		super(msg);
	}

	public RegisterAccountException(Throwable th) {
		super(th);
	}

	public RegisterAccountException(String msg, Throwable th) {
		super(msg, th);
	}

	public RegisterAccountException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public RegisterAccountException(int errorCode, String errorMessage, String source) {
		super(errorCode, errorMessage, source);
	}
}
