package com.truemoney.ewallet.businesslogic.exception;

public class IntegrationException extends PaymentServiceException {

	private static final long serialVersionUID = 1L;

	public IntegrationException(String msg) {
		super(msg);
	}

	public IntegrationException(Throwable th) {
		super(th);
	}

	public IntegrationException(String msg, Throwable th) {
		super(msg, th);
	}

	public IntegrationException(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public IntegrationException(int errorCode, String errorMessage, String source) {
		super(errorCode, errorMessage, source);
	}
}
