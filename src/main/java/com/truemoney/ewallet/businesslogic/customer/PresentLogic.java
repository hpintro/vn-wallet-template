package com.truemoney.ewallet.businesslogic.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.truemoney.ewallet.persistence.dao.api.factory.DaoFactory;
import com.truemoney.ewallet.persistence.model.customer.UserPresent;

@Service
@Transactional
public class PresentLogic {
	@Autowired
	DaoFactory daoFactory;
	
	public String redeem(long receiver, long presentId) {
		UserPresent userPresent = daoFactory.getUserPresentDAO().getByPresentId(presentId);
		if(userPresent == null)
			return "Fail!";
		userPresent.setReceiver("" + receiver);
		
		daoFactory.getUserPresentDAO().update(userPresent, 0L);
		return "success!";
	}
}
