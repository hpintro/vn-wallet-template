package com.truemoney.ewallet.businesslogic.customer;

import com.truemoney.ewallet.persistence.model.customer.Customer;

public interface ICustomerLogic {

	Customer getCustomerById(Long customerId);

}
