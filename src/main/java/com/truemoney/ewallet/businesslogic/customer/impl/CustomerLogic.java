package com.truemoney.ewallet.businesslogic.customer.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.truemoney.ewallet.businesslogic.customer.ICustomerLogic;
import com.truemoney.ewallet.persistence.dao.api.factory.DaoFactory;
import com.truemoney.ewallet.persistence.model.customer.Customer;

@Service
@Transactional
public class CustomerLogic implements ICustomerLogic, InitializingBean {
	private static Logger logger = Logger.getLogger(CustomerLogic.class);
	@Autowired
	private DaoFactory daoFactory;

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		if (this.daoFactory == null)
			throw new IllegalStateException("daoFactory is required");
	}

	@Override
	public Customer getCustomerById(Long customerId) {
		// TODO Auto-generated method stub
		return daoFactory.getCustomerDAO().getById(customerId);
	}

}
