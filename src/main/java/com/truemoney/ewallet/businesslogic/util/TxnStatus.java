package com.truemoney.ewallet.businesslogic.util;

public class TxnStatus {
	public static final int INITIAL = 0;
	public static final int PRE_AUTHORIZED = 2;
	public static final int AUTH_REQUIRED_BOTH = 5;
	public static final int AUTH_REQUIRED_PAYER = 6;
	public static final int AUTH_REQUIRED_PAYEE = 7;
	public static final int AUTHENTICATED = 10;
	public static final int DECISION_WAITING = 15;
	public static final int AUTHORIZED = 20;
	public static final int CAPTURED = 30;
	public static final int DELIVERED = 40;
	public static final int ACKNOWLEGED = 50;
	public static final int AUTHORIZATION_CANCELED = 60;
	public static final int CAPTURE_CANCELED = 70;
	public static final int FULLY_PROCESSED = 99;
}
