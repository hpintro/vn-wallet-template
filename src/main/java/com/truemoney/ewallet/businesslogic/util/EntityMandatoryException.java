package com.truemoney.ewallet.businesslogic.util;

import vn.mog.framework.service.api.MobiliserServiceException;

public class EntityMandatoryException extends MobiliserServiceException {
	private static final long serialVersionUID = 1L;

	public EntityMandatoryException() {
	}

	public EntityMandatoryException(String message) {
		super(message);
	}

	public EntityMandatoryException(Throwable cause) {
		super(cause);
	}

	public EntityMandatoryException(String message, Throwable cause) {
		super(message, cause);
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_GENERIC_ENTITY_MANDATORY;
	}
}
