package com.truemoney.ewallet.businesslogic.util;

import java.util.Collections;

import vn.mog.framework.service.api.MobiliserServiceException;

public class EntityNotFoundException extends MobiliserServiceException {
	private static final String MISSING_ENTITY = "MISSING_ENTITY";
	private static final long serialVersionUID = 1L;

	public EntityNotFoundException() {
	}

	public EntityNotFoundException(String message, String entity) {
		super(message, Collections.singletonMap(MISSING_ENTITY, entity));
	}

	public EntityNotFoundException(Throwable cause, String entity) {
		super(cause, Collections.singletonMap(MISSING_ENTITY, entity));
	}

	public EntityNotFoundException(String message, Throwable cause, String entity) {
		super(message, cause, Collections.singletonMap(MISSING_ENTITY, entity));
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_GENERIC_ENTITY_NOT_FOUND;
	}
}
