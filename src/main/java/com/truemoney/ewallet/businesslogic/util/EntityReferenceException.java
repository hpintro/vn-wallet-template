package com.truemoney.ewallet.businesslogic.util;

import java.util.Collections;

import vn.mog.framework.service.api.MobiliserServiceException;

public class EntityReferenceException extends MobiliserServiceException {
	private static final String ENTITY_REFERENCED = "ENTITY_REFERENCED";
	private static final long serialVersionUID = 1L;

	public EntityReferenceException() {
	}

	public EntityReferenceException(String message, String entity) {
		super(message, Collections.singletonMap(ENTITY_REFERENCED, entity));
	}

	public EntityReferenceException(Throwable cause, String entity) {
		super(cause, Collections.singletonMap(ENTITY_REFERENCED, entity));
	}

	public EntityReferenceException(String message, Throwable cause, String entity) {
		super(message, cause, Collections.singletonMap(ENTITY_REFERENCED, entity));
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_ENTITY_REFERENCED;
	}
}
