package com.truemoney.ewallet.businesslogic.util;

import java.util.HashMap;
import java.util.Map;

import vn.mog.framework.service.api.MobiliserServiceException;

public class EntityWrongStatusException extends MobiliserServiceException {
	private static final String ENTITY = "ENTITY";
	private static final String CURRENT_STATUS = "CURRENT_STATUS";
	private static final long serialVersionUID = 1L;

	public EntityWrongStatusException() {
	}

	public EntityWrongStatusException(String message, String entity, String status) {
		super(message, makeMap(entity, status));
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_ENTITY_WRONG_STATUS;
	}

	private static Map<String, String> makeMap(String entity, String status) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(ENTITY, entity);
		map.put(CURRENT_STATUS, status);
		return map;
	}
}
