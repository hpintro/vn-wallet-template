package com.truemoney.ewallet.businesslogic.util;

import vn.mog.framework.service.api.MobiliserServiceException;

public class UserAccessingNotOwnedResourceException extends MobiliserServiceException {
	private static final long serialVersionUID = -4055951957187488922L;

	public UserAccessingNotOwnedResourceException(String message) {
		super(message);
	}

	public UserAccessingNotOwnedResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserAccessingNotOwnedResourceException(Throwable cause) {
		super(cause);
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_CODE_INVALID_RESOURCE_ACCESS;
	}
}
