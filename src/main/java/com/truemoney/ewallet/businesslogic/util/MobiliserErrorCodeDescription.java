package com.truemoney.ewallet.businesslogic.util;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ java.lang.annotation.ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MobiliserErrorCodeDescription {
	public InfoMode infoLevel();

	public String description();

	public String name();

	public static enum InfoMode {
		INFO, DEBUG, WARN, ERROR, FATAL;
	}
}
