package com.truemoney.ewallet.businesslogic.util;

@SuppressWarnings("unused")
public class StatusCodes {

	@MobiliserErrorCodeDescription(name = "OK", description = "indicates no error occured", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int OK = 0;
	private static final int ERROR_GROUP_FRAMEWORK = 50;

	@MobiliserErrorCodeDescription(name = "ERROR_TRACING_REPEAT_FLAG_MISSING", description = "The request's repeat flag is missing.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRACING_REPEAT_FLAG_MISSING = 51;

	@MobiliserErrorCodeDescription(name = "ERROR_TRACING_STILL_IN_PROCESS", description = "The request is still being processed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRACING_STILL_IN_PROCESS = 52;

	@MobiliserErrorCodeDescription(name = "ERROR_TRACING_WRONG_USER_FOR_REPEAT", description = "The user sending a repeat request is invalid.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRACING_WRONG_USER_FOR_REPEAT = 53;

	@MobiliserErrorCodeDescription(name = "ERROR_TRACING_WRONG_USER_FOR_REPEAT", description = "No message found for a repeated request.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRACING_REPEAT_SET_BUT_NO_MESSAGE_FOUND = 54;

	@MobiliserErrorCodeDescription(name = "ERROR_TRACING_NO_RESPONSE_CACHED", description = "No response was cached for a request.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRACING_NO_RESPONSE_CACHED = 55;
	private static final int ERROR_GROUP_CONFIGURATION = 100;

	@MobiliserErrorCodeDescription(name = "ERROR_CONFIGURATION_CUSTOMER_INVALID", description = "The error code indicating that a customer is set up invalid or incomplete.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CONFIGURATION_CUSTOMER_INVALID = 101;

	@MobiliserErrorCodeDescription(name = "ERROR_LICENSE", description = "The error code indicating that some license issues prevent execution.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_LICENSE = 191;

	@MobiliserErrorCodeDescription(name = "ERROR_CONFIGURATION_DATABASE", description = "The error code indicating that some assumptions about the database are wrong, the database is inconsistent or misconfigured.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CONFIGURATION_DATABASE = 199;
	private static final int ERROR_GROUP_GENERIC = 200;

	@MobiliserErrorCodeDescription(name = "ERROR_GENERIC_ENTITY_NOT_FOUND", description = "No entity found for the given identifier.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_GENERIC_ENTITY_NOT_FOUND = 201;

	@MobiliserErrorCodeDescription(name = "ERROR_GENERIC_ENTITY_NOT_UNIQUE", description = "There already exists an entity with the same identifier", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_GENERIC_ENTITY_NOT_UNIQUE = 202;

	@MobiliserErrorCodeDescription(name = "ERROR_GENERIC_ENTITY_MANDATORY", description = "Invalid data, an identifier or a whole entity is missing.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_GENERIC_ENTITY_MANDATORY = 203;

	@MobiliserErrorCodeDescription(name = "ERROR_GENERIC_ILLEGAL_DATA", description = "Invlid data was passed to the unit of work.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_GENERIC_ILLEGAL_DATA = 204;

	@MobiliserErrorCodeDescription(name = "ERROR_GENERIC_ACCESS_DENIED", description = "The calling user has not the required access privilege.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_GENERIC_ACCESS_DENIED = 205;

	@MobiliserErrorCodeDescription(name = "ERROR_ENTITY_REFERENCED", description = "An entity could not be deleted, because it is still referenced.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_ENTITY_REFERENCED = 206;

	@MobiliserErrorCodeDescription(name = "ERROR_ENTITY_WRONG_STATUS", description = "The given entity is not in the correct status for the operation.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_ENTITY_WRONG_STATUS = 207;

	@MobiliserErrorCodeDescription(name = "ERROR_ENTITY_WRONG_STATUS", description = "This error code depicts that any process is being suspended and waiting for further approval", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PENDING_FOR_APPROVAL = 250;

	@MobiliserErrorCodeDescription(name = "ERROR_PENDING_TASK_NOT_FOUND", description = "This error code depicts that the requested task could not be resumed because it not be found", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PENDING_TASK_NOT_FOUND = 451;

	@MobiliserErrorCodeDescription(name = "ERROR_PENDING_TASK_CLAIMED", description = "This error code depicts that the requested task could not be resumed because it is already claimed", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PENDING_TASK_CLAIMED = 452;

	@MobiliserErrorCodeDescription(name = "ERROR_PENDING_START_MISSING_PRIVILEGE", description = "This error code depicts that the user tried to perform an operation but is missing the maker privilege", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PENDING_START_MISSING_PRIVILEGE = 453;

	@MobiliserErrorCodeDescription(name = "ERROR_PENDING_CONTINUE_MISSING_PRIVILEGE", description = "This error code depicts that the user tried to perform an operation but is missing the checker privilege", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PENDING_CONTINUE_MISSING_PRIVILEGE = 454;
	public static final int ERROR_GENERIC_BL = 299;
	private static final int ERROR_GROUP_CUSTOMER = 300;

	@MobiliserErrorCodeDescription(name = "ERROR_CUSTOMER_INACTIVE", description = "The customer is inactive.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CUSTOMER_INACTIVE = 301;

	@MobiliserErrorCodeDescription(name = "ERROR_CUSTOMER_BLACKLISTED", description = "The customer is blacklisted.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CUSTOMER_BLACKLISTED = 302;

	@MobiliserErrorCodeDescription(name = "ERROR_CUSTOMER_NOT_FOUND", description = "The requested customer was not found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CUSTOMER_NOT_FOUND = 303;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_EXPIRED", description = "The customer's credential is expired.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_EXPIRED = 321;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_BLOCKED", description = "The customer's credential is blocked.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_BLOCKED = 322;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_INVALID_PATTERN", description = "The credential has an invalid pattern.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_INVALID_PATTERN = 323;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_WEAK_BLOCK", description = "The credential contains a weak block.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_WEAK_BLOCK = 324;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_INVALID", description = "The credential is invalid.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_INVALID = 325;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_TOO_LONG", description = "The credential was not found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_NOT_FOUND = 326;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_TOO_LONG", description = "The credential is too long.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_TOO_LONG = 327;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_TOO_SHORT", description = "The credential is too short.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_TOO_SHORT = 328;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_WRONG", description = "The credential wrong.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_WRONG = 329;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_WRONG_AND_ACTION_TAKEN", description = "The credential is worng and the customer was blacklisted.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_WRONG_AND_ACTION_TAKEN = 330;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_OK_BUT_EXPIRED", description = "The credential is ok, but expired.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_OK_BUT_EXPIRED = 331;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_OK_BUT_TEMPORARY", description = "The credential is ok, but temporary.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_OK_BUT_TEMPORARY = 332;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_NO_POLICY", description = "Configuration issue, no security policy found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_NO_POLICY = 333;

	@MobiliserErrorCodeDescription(name = "ERROR_CREDENTIAL_RETENTION_MATCH", description = "The credential can not be set, because it is still being retained.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CREDENTIAL_RETENTION_MATCH = 334;

	@MobiliserErrorCodeDescription(name = "ERROR_SESSION_TOO_MANY", description = "The many active sessions.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_SESSION_TOO_MANY = 351;

	@MobiliserErrorCodeDescription(name = "ERROR_SESSION_CLOSED", description = "The session was already closed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_SESSION_CLOSED = 352;

	@MobiliserErrorCodeDescription(name = "ERROR_SESSION_EXPIRED", description = "The session is expired.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_SESSION_EXPIRED = 353;

	@MobiliserErrorCodeDescription(name = "ERROR_SESSION_NOT_SUPPORTED", description = "Session not supported.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_SESSION_NOT_SUPPORTED = 354;

	@MobiliserErrorCodeDescription(name = "ERROR_SESSION_NOT_FOUND", description = "No active session found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_SESSION_NOT_FOUND = 355;

	@MobiliserErrorCodeDescription(name = "ERROR_PERSISTENT_LOGIN_TOO_MANY", description = "To many logins.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PERSISTENT_LOGIN_TOO_MANY = 361;

	@MobiliserErrorCodeDescription(name = "ERROR_PERSISTENT_LOGIN_STOLEN", description = "The login has been stolen.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PERSISTENT_LOGIN_STOLEN = 362;

	@MobiliserErrorCodeDescription(name = "ERROR_PERSISTENT_LOGIN_EXPIRED", description = "The login expired.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PERSISTENT_LOGIN_EXPIRED = 363;

	@MobiliserErrorCodeDescription(name = "ERROR_PERSISTENT_LOGIN_NOT_SUPPORTED", description = "Login not supported.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PERSISTENT_LOGIN_NOT_SUPPORTED = 364;

	@MobiliserErrorCodeDescription(name = "ERROR_CUSTOMER_BL", description = "The customer is blacklisted.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CUSTOMER_BL = 399;
	private static final int ERROR_GROUP_CUSTOMER_DATA = 400;

	@MobiliserErrorCodeDescription(name = "ERROR_CUSTOMER_DATA_MISSING", description = "The customer data is missing", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CUSTOMER_DATA_MISSING = 401;

	@MobiliserErrorCodeDescription(name = "ERROR_CUSTOMER_MSISDN_MISSING", description = "The customer has no active MSISDN.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CUSTOMER_MSISDN_MISSING = 402;

	@MobiliserErrorCodeDescription(name = "ERROR_CUSTOMER_EMAIL_MISSING", description = "The customer has no active email.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CUSTOMER_EMAIL_MISSING = 403;
	private static final int ERROR_GROUP_VIRUS = 700;

	@MobiliserErrorCodeDescription(name = "ERROR_VIRUS_INFECTION", description = "The virus scanner reports an virus infection in the binary data.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_VIRUS_INFECTION = 701;

	@MobiliserErrorCodeDescription(name = "ERROR_VIRUS_SCAN", description = "The virus scanner reports an scan error.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_VIRUS_SCAN = 702;

	@MobiliserErrorCodeDescription(name = "ERROR_VIRUS_SYSTEM", description = "The virus scanner reports an syste, error.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_VIRUS_SYSTEM = 703;
	private static final int ERROR_GROUP_REQUEST_PROCESSING = 1000;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_INVALID", description = "The error code indicating that a request is incomplete or invalid; usually this refers to artifacts that cannot be covered by XML Schemas. It also indicates a invalid logic in the request fields, such as minimum amount greater than max amount", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_INVALID = 1001;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_INVALID_MSISDN", description = "The error code indicating that an invalid MSISDN is specified.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_INVALID_MSISDN = 1002;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_UNKNOWN_TXN", description = "The error code indicating that an unknown transaction is specified.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_UNKNOWN_TXN = 1003;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_INVALID_TXN", description = "The error code indicating that a transaction is specified that cannot be considered in the request context.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_INVALID_TXN = 1004;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_INVALID_TXN_STATUS", description = "The error code indicating that a transaction is specified that cannot be considered due to its status.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_INVALID_TXN_STATUS = 1005;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_INACTIVE_CUSTOMER", description = "The error code indicating that a customer is specified whom is inactive.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_INACTIVE_CUSTOMER = 1006;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_TEST_LIVE_MIX", description = "The error code indicating that either a <i>live</i> customer tried to participate in a <i>test</i> transaction or vice versa.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_TEST_LIVE_MIX = 1007;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_BLACKLISTED_CUSTOMER", description = "The error code indicating that a customer is specified whom is blacklisted.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_BLACKLISTED_CUSTOMER = 1008;

	@MobiliserErrorCodeDescription(name = "ERROR_TRANSACTION_REPETITION", description = "The error code indicating that a request with the same origin and trace number was executed within the last 24 hours.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRANSACTION_REPETITION = 1009;

	@MobiliserErrorCodeDescription(name = "ERROR_REQUEST_UNKNOWN_CUSTOMER", description = "The error code indicating that an unknown customer is specified.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REQUEST_UNKNOWN_CUSTOMER = 1012;

	@MobiliserErrorCodeDescription(name = "ERROR_TRANSACTION_LOCKED", description = "The error code indicating that the transaction is locked by another process and cannot be handled right now.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRANSACTION_LOCKED = 1050;
	private static final int ERROR_GROUP_AUTHENTICATION = 1100;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_MISSING", description = "The error code indicating that authentication data is missing.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_MISSING = 1101;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_INVALID", description = "The error code indicating that authentication data is invalid; e.g. protocol violations or unknown/invalid encryption is used.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_INVALID = 1102;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_UNKNOWN_USER", description = "The error code indicating that the authentication failed because the user does not exist.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_UNKNOWN_USER = 1111;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_INACTIVE_USER", description = "The error code indicating that the authentication failed because the user is in a status prohibiting to execute anything.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_INACTIVE_USER = 1112;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_INVALID_CREDENTIALS", description = "The error code indicating that the authentication failed because the credentials are incorrect.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_INVALID_CREDENTIALS = 1113;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_BLOCKED_USER", description = "The error code indicating that the authentication failed because the user is blocked due to the number of times wrong credentials have been specified.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_BLOCKED_USER = 1114;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_BLACKLISTED", description = "The error code indicating that the authentication failed because the user is blacklisted.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_BLACKLISTED = 1115;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_INVALID_AND_BLOCKED", description = "The error code indicating that the authentication failed because the credentials are incorrect and the customer is blocked as of now.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_INVALID_AND_BLOCKED = 1116;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_INVALID_AND_BLACKLISTED", description = "The error code indicating that the authentication failed because the credentials are incorrect and the customer is blacklisted as of now.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_INVALID_AND_BLACKLISTED = 1117;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHENTICATION_CREDENTIALS_EXPIRED", description = "The error code indicating that the credentials have expired and need to be updated.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHENTICATION_CREDENTIALS_EXPIRED = 1121;
	private static final int ERROR_GROUP_AUTHORISATION = 1200;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHORISATION_NO_AGENT", description = "The error code indicating that the authorisation failed because no agent/user data is given.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHORISATION_NO_AGENT = 1201;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHORISATION_INACTIVE_AGENT", description = "The error code indicating that the authorisation failed because the agent/user is in a status prohibiting to execute anything.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHORISATION_INACTIVE_AGENT = 1202;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHORISATION_LOGIN_NOT_ALLOWED", description = "The error code idicating that the indentified customer/agent is not allowed to create a session or login. One reason could be that it is an internal customer", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHORISATION_LOGIN_NOT_ALLOWED = 1203;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHORISATION_FAILED", description = "The error code indicating that the authorisation failed because the agent misses at least one privilege.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHORISATION_FAILED = 1211;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHORISATION_INVALID_ACCESS", description = "The error code indicating that a basically authorised access to an unauthorised resource was detected.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHORISATION_INVALID_ACCESS = 1221;

	@MobiliserErrorCodeDescription(name = "ERROR_CODE_INVALID_RESOURCE_ACCESS", description = "The error code indicating that an authorised user requested a resource which does not belong to him and for which he is not authorised.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CODE_INVALID_RESOURCE_ACCESS = 1222;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTO_CAPTURE_REQUIRED", description = "The transactions use case requires auto capture but the transaction is not", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTO_CAPTURE_REQUIRED = 1231;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTO_CAPTURE_NOT_ALLOWED", description = "The transactions use case permitts auto capture but the transaction is an autocapture one", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTO_CAPTURE_NOT_ALLOWED = 1232;

	@MobiliserErrorCodeDescription(name = "ERROR_TRANSACTION_EXPIRED", description = "The transaction was pending in initial state too long and invalidated automatically.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRANSACTION_EXPIRED = 1250;
	private static final int ERROR_GROUP_REMOTE = 1300;

	@MobiliserErrorCodeDescription(name = "ERROR_REMOTE_SESSION_ACTIVE", description = "The error code indicating that a login failed because there is still an active session.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REMOTE_SESSION_ACTIVE = 1301;

	@MobiliserErrorCodeDescription(name = "ERROR_MAX_SESSIONS_REACHED", description = "The error code indicating that the customer has reached his limit of parallel sessions", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_MAX_SESSIONS_REACHED = 1302;

	@MobiliserErrorCodeDescription(name = "ERROR_REMOTE_UPDATE", description = "The error code indicating that the used application needs to be updated.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_REMOTE_UPDATE = 1391;
	private static final int ERROR_GROUP_BULKPROCESSING = 1500;

	@MobiliserErrorCodeDescription(name = "ERROR_BULK_PROCESSING_NO_FILE_HANDLER", description = "Indicates that there was no file handler found for a specific file type.", infoLevel = MobiliserErrorCodeDescription.InfoMode.WARN)
	public static final int ERROR_BULK_PROCESSING_NO_FILE_HANDLER = 1501;

	@MobiliserErrorCodeDescription(name = "ERROR_BULK_PROCESSING_INVALID_LINE", description = "The error code indicating an invalid line was encountered during the file processing", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_BULK_PROCESSING_INVALID_LINE = 1502;

	@MobiliserErrorCodeDescription(name = "ERROR_BULK_PROCESSING_IO", description = "The error code indicating an I/O exception while reading the file", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_BULK_PROCESSING_IO = 1503;

	@MobiliserErrorCodeDescription(name = "ERROR_BULK_PROCESSING_GENERAL", description = "The error code indicating that something went wrong during bulk processing.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_BULK_PROCESSING_GENERAL = 1599;
	private static final int ERROR_GROUP_NOTIFICATIONS = 2400;

	@MobiliserErrorCodeDescription(name = "ERROR_BULK_PROCESSING_GENERAL", description = "The error code indicating that the connection to the gw-msg failed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NOTIFICATIONS_CONNECT_FAILURE = 2401;

	@MobiliserErrorCodeDescription(name = "ERROR_NOTIFICATIONS_CONNECT_TIMEOUT", description = "The error code indicating that the connection to the gw-msg timed out.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NOTIFICATIONS_CONNECT_TIMEOUT = 2402;

	@MobiliserErrorCodeDescription(name = "ERROR_NOTIFICATIONS_READ_TIMEOUT", description = "The error code indicating that reading data from the gw-msg timed out.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NOTIFICATIONS_READ_TIMEOUT = 2403;

	@MobiliserErrorCodeDescription(name = "ERROR_NOTIFICATIONS_PROTOCOL", description = "The error code indicating that the gw-msg send data that is unexpected or not fitting the protocol.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NOTIFICATIONS_PROTOCOL = 2404;

	@MobiliserErrorCodeDescription(name = "ERROR_NOTIFICATIONS_SEND", description = "The error code indicating that sending a message via gw-msg failed: communication with gw-msg was ok, but gw-msg indicated an error.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NOTIFICATIONS_SEND = 2405;

	@MobiliserErrorCodeDescription(name = "ERROR_NOTIFICATION_TYPE_NOT_SUPPORTED", description = "The specified notification type (e.g. email, sms) is not supported.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NOTIFICATION_TYPE_NOT_SUPPORTED = 2406;
	public static final int ERROR_GROUP_PAYER_PI = 2600;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_INACTIVE", description = "The error code indicating that the payer payment instrument is inactive.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_INACTIVE = 2601;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_IO", description = "The error code indicating that the communication with the payment instrument's processor failed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_IO = 2602;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_INCONSISTENCY", description = "The error code indicating that there is an inconsistency between the payer payment instrument's processor and the Money Mobiliser.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_INCONSISTENCY = 2603;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_WRONG_STATUS", description = "The payer payment instrument is in a wrong status for the current request.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_WRONG_STATUS = 2604;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_NOT_ALLOWED", description = "The payer payment instrument is not allowed for this use case.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_NOT_ALLOWED = 2605;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_WEEKLY_LIMIT", description = "The error code indicating that the payer payment instrument's weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_WEEKLY_LIMIT = 2606;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_DAILY_LIMIT", description = "The error code indicating that the payer payment instrument's daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_DAILY_LIMIT = 2607;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_MONTHLY_LIMIT", description = "The error code indicating that the payer payment instrument's monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_MONTHLY_LIMIT = 2608;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT", description = "The error code indicating that the payer payment instrument's absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT = 2609;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_SINGLE_LIMIT", description = "The error code indicating that the payer payment instrument's single txn limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_SINGLE_LIMIT = 2610;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_NO_FUNDS", description = "The error code indicating that the payer payment instrument has no funds available.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_NO_FUNDS = 2611;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_WEEKLY_LIMIT_COUNT", description = "The error code indicating that the payer payment instrument's weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_WEEKLY_LIMIT_COUNT = 2612;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_DAILY_LIMIT_COUNT", description = "The error code indicating that the payer payment instrument's daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_DAILY_LIMIT_COUNT = 2613;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_MONTHLY_LIMIT_COUNT", description = "The error code indicating that the payer payment instrument's monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_MONTHLY_LIMIT_COUNT = 2614;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT_COUNT", description = "The error code indicating that the payer payment instrument's absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT_COUNT = 2615;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_SINGLE_MINIMUM_UNDERRUN", description = "The error code indicating that the payer payment instrument's single minimum limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_SINGLE_MINIMUM_UNDERRUN = 2616;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_EXPIRED", description = "The error code indicating that the authorisation of the payment instrument's processor has expired.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_EXPIRED = 2621;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_CANCEL_CAPTURE_AMOUNT", description = "The error code indicating that a capture cancel with this payment instrument's processor can only cancel the full capture amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_CANCEL_CAPTURE_AMOUNT = 2622;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_CAPTURE_FROZEN", description = "The error code indicating that the capture of the payer payment instrument's processor has \"frozen\" and can no longer be cancelled.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_CAPTURE_FROZEN = 2623;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_BLOCKED", description = "The error code indicating that the payer payment instrument is blocked on the issuer side (blacklisted).", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_BLOCKED = 2631;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_CVD_FAILED", description = "The error code indicating that the CVD validation of the payment instrument failed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_CVD_FAILED = 2641;

	@MobiliserErrorCodeDescription(name = "ERROR_PICKUP_NO_CODE", description = "The error code indicating that the transaction has no pickup code attached.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PICKUP_NO_CODE = 2650;

	@MobiliserErrorCodeDescription(name = "ERROR_PICKUP_MAX_TRIES", description = "The error code indicating that the maximum number of tries has been exceeded.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PICKUP_MAX_TRIES = 2651;

	@MobiliserErrorCodeDescription(name = "ERROR_PICKUP_INVALID_CODE", description = "The error code indicating that the pickup code is invalid.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PICKUP_INVALID_CODE = 2652;

	@MobiliserErrorCodeDescription(name = "ERROR_PICKUP_ATTRIBUTE_NOT_UNIQUE", description = "The error code indicating that the pickup code attribute attached to the transaction is not unique.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PICKUP_ATTRIBUTE_NOT_UNIQUE = 2653;

	@MobiliserErrorCodeDescription(name = "ERROR_PICKUP_INTERNAL_VOUCHER_NOT_FOUND", description = "The error code indicating that no internal voucher associated with the transaction can be found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PICKUP_INTERNAL_VOUCHER_NOT_FOUND = 2654;

	@MobiliserErrorCodeDescription(name = "ERROR_PICKUP_INTERNAL_VOUCHER_TXN_NOT_FOUND", description = "The error code indicating that no internal voucher transaction associated with the transactionId can be found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PICKUP_INTERNAL_VOUCHER_TXN_NOT_FOUND = 2655;

	@MobiliserErrorCodeDescription(name = "ERROR_PICKUP_INTERNAL_VOUCHER_INACTIVE", description = "The error code indicating that no internal voucher transaction associated with the transactionId can be found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PICKUP_INTERNAL_VOUCHER_INACTIVE = 2656;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLETENTRY_INVALID", description = "Generic error code indicating that the wallet entry is invalid", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLETENTRY_INVALID = 2660;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLETENTRY_INVALID_ALIAS", description = "The error code indicating that a wallet entry exists for the same customer with a different payer payment instrument", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLETENTRY_INVALID_ALIAS = 2661;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLETENTRY_INVALID_PRIORITY", description = "The error code indicating that a wallet entry exists for the same customer having the same debit or credit priority", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLETENTRY_INVALID_PRIORITY = 2662;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_NO_BALANCE", description = "The error code indicating that the payer payment instrument does not support the <i>Balance Inquiry</i> operation.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_NO_BALANCE = 2691;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_INVALID", description = "The error code indicating that the payer payment instrument configuration is invalid.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_INVALID = 2697;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PAYMENTINSTRUMENT_ERROR", description = "The error code indicating that the payer payment instrument processor returned an unknown error.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PAYMENTINSTRUMENT_ERROR = 2699;
	public static final int ERROR_GROUP_PAYEE_PI = 2700;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_INACTIVE", description = "The error code indicating that the payee payment instrument is inactive.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_INACTIVE = 2701;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_IO", description = "The error code indicating that the communication with the payment instrument's processor failed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_IO = 2702;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_INCONSISTENCY", description = "The error code indicating that there is an inconsistency between the payee payment instrument's processor and the Money Mobiliser.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_INCONSISTENCY = 2703;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_WRONG_STATUS", description = "The payee payment instrument is in a wrong status for the current request.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_WRONG_STATUS = 2704;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_NOT_ALLOWED", description = "The payee payment instrument is not allowed for this use case.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_NOT_ALLOWED = 2705;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_WEEKLY_LIMIT", description = "The error code indicating that the payee payment instrument's weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_WEEKLY_LIMIT = 2706;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_DAILY_LIMIT", description = "The error code indicating that the payee payment instrument's daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_DAILY_LIMIT = 2707;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_MONTHLY_LIMIT", description = "The error code indicating that the payee payment instrument's monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_MONTHLY_LIMIT = 2708;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT", description = "The error code indicating that the payee payment instrument's absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT = 2709;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_SINGLE_LIMIT", description = "The error code indicating that the payee payment instrument's single txn limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_SINGLE_LIMIT = 2710;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_NO_FUNDS", description = "The error code indicating that the payee payment instrument has no funds available.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_NO_FUNDS = 2711;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_WEEKLY_LIMIT_COUNT", description = "The error code indicating that the payee payment instrument's weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_WEEKLY_LIMIT_COUNT = 2712;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_DAILY_LIMIT_COUNT", description = "The error code indicating that the payee payment instrument's daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_DAILY_LIMIT_COUNT = 2713;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_MONTHLY_LIMIT_COUNT", description = "The error code indicating that the payee payment instrument's monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_MONTHLY_LIMIT_COUNT = 2714;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT_COUNT", description = "The error code indicating that the payee payment instrument's absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_ABSOLUTE_LIMIT_COUNT = 2715;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_SINGLE_MINIMUM_UNDERRUN", description = "The error code indicating that the payee payment instrument's single minimum limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_SINGLE_MINIMUM_UNDERRUN = 2716;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_EXPIRED", description = "The error code indicating that the authorisation of the payment instrument's processor has expired.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_EXPIRED = 2721;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_CANCEL_CAPTURE_AMOUNT", description = "The error code indicating that a capture cancel with this payment instrument's processor can only cancel the full capture amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_CANCEL_CAPTURE_AMOUNT = 2722;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_CAPTURE_FROZEN", description = "The error code indicating that the capture of the payee payment instrument's processor has \"frozen\" and can no longer be cancelled.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_CAPTURE_FROZEN = 2723;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_BLOCKED", description = "The error code indicating that the payee payment instrument is blocked on the issuer side (blacklisted).", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_BLOCKED = 2731;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_CVD_FAILED", description = "The error code indicating that the CVD validation of the payment instrument failed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_CVD_FAILED = 2741;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLETENTRY_INVALID", description = "Generic error code indicating that the payee wallet entry is invalid", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLETENTRY_INVALID = 2760;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLETENTRY_INVALID_ALIAS", description = "The error code indicating that a payee wallet entry exists for the same customer with a different payee payment instrument", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLETENTRY_INVALID_ALIAS = 2761;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLETENTRY_INVALID_PRIORITY", description = "The error code indicating that a payee wallet entry exists for the same customer having the same debit or credit priority", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLETENTRY_INVALID_PRIORITY = 2762;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_NO_BALANCE", description = "The error code indicating that the payee payment instrument does not support the <i>Balance Inquiry</i> operation.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_NO_BALANCE = 2791;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYMENTINSTRUMENT_ERROR", description = "The error code indicating that the payee payment instrument processor returned an unknown error.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYMENTINSTRUMENT_ERROR = 2799;
	private static final int ERROR_GROUP_MONEY = 2500;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_UNKNOWN", description = "The error code indicating that the payee is unknown.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_UNKNOWN = 2501;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_INVALID", description = "The error code indicating that the payee is invalid / missing privileges", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_INVALID = 2502;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PI_NONE", description = "The error code indicating that the payee has no PI.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PI_NONE = 2503;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PI_NO_CREDIT", description = "The error code indicating that the payee PI does not support <i>Credit</i>.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PI_NO_CREDIT = 2505;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PI_UNKNOWN", description = "The error code indicating that the payee PI is unknown.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PI_UNKNOWN = 2507;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_INVALID_DELEGATE", description = "The error code indicating that the caller cannot act as a delegate for the payee.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_INVALID_DELEGATE = 2508;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_UNKNOWN", description = "The error code indicating that the payer is unknown.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_UNKNOWN = 2511;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_INVALID", description = "The error code indicating that the payer is invalid/missing privilege.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_INVALID = 2512;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PI_NONE", description = "The error code indicating that the payer has no PI.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PI_NONE = 2513;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PI_NO_DEBIT", description = "The error code indicating that the payer PI does not support <i>Debit</i>.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PI_NO_DEBIT = 2515;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PI_NOT_SUPPORTED", description = "The error code indicating that the payer PI is not supported.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PI_NOT_SUPPORTED = 2516;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_PI_UNKNOWN", description = "The error code indicating that the payer PI is unknown.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_PI_UNKNOWN = 2517;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_INVALID_DELEGATE", description = "The error code indicating that the caller cannot act as a delegate for the payer.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_INVALID_DELEGATE = 2518;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTH_EXPIRED", description = "The error code indicating that an authorization expired.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTH_EXPIRED = 2519;

	@MobiliserErrorCodeDescription(name = "ERROR_TXN_AUTH_REQUIRED", description = "The error code indicating that an transaction authentication is required for the transaction.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TXN_AUTH_REQUIRED = 2521;

	@MobiliserErrorCodeDescription(name = "ERROR_TXN_AUTH_WRONG", description = "The error code indicating that a wrong OTP was specified.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TXN_AUTH_WRONG = 2522;

	@MobiliserErrorCodeDescription(name = "ERROR_TXN_AUTH_WRONG_FINAL", description = "The error code indicating that a wrong OTP was specified and the transaction failed permanently.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TXN_AUTH_WRONG_FINAL = 2524;

	@MobiliserErrorCodeDescription(name = "ERROR_TXN_MANUAL_DECISION_REQUIRED", description = "Indicates that the transaction processing has been stopped, because a manual decision is required", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TXN_MANUAL_DECISION_REQUIRED = 2525;

	@MobiliserErrorCodeDescription(name = "ERROR_CAPTURE_AMOUNT", description = "The error code indicating that the capture amount exceeds the reservation amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CAPTURE_AMOUNT = 2531;

	@MobiliserErrorCodeDescription(name = "ERROR_CAPTURE_CANCEL_AMOUNT", description = "The error code indicating that the capture cancel amount exceeds the capture amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_CAPTURE_CANCEL_AMOUNT = 2532;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_PAYER_IDENTICAL", description = "The error code indicating that the transaction is not allowed because payee and payer are the same customer.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_PAYER_IDENTICAL = 2533;

	@MobiliserErrorCodeDescription(name = "ERROR_AMOUNT_DIFFERS", description = "The error code indicating that the sub transaction amount differs from the original transaction amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AMOUNT_DIFFERS = 2534;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_OPEN_TRANSACTION", description = "The error code indicating that the payee has another open transaction and thus cannot open a new one.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_OPEN_TRANSACTION = 2535;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_OPEN_TRANSACTION", description = "The error code indicating that the payer has another open transaction and thus cannot open a new one.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_OPEN_TRANSACTION = 2536;

	@MobiliserErrorCodeDescription(name = "ERROR_NO_OPEN_TRANSACTION", description = "The error code indicating that no open transaction was found.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NO_OPEN_TRANSACTION = 2537;

	@MobiliserErrorCodeDescription(name = "ERROR_MULTIPLE_OPEN_TRANSACTIONS", description = "The error code indicating that the customer has multiple open transactions", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_MULTIPLE_OPEN_TRANSACTIONS = 2538;

	@MobiliserErrorCodeDescription(name = "ERROR_BACKSTOP_FAILED", description = "The error code indicating that the backstop authorisation failed", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_BACKSTOP_FAILED = 2539;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_ABSOLUTE_LIMIT_COUNT", description = "The error code indicating that the payee's wallet absolute limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_ABSOLUTE_LIMIT_COUNT = 2540;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHORIZATION_PAYER_DENIED", description = "The error code indicating that the payer denied the transaction during the authorization process.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHORIZATION_PAYER_DENIED = 2541;

	@MobiliserErrorCodeDescription(name = "ERROR_AUTHORIZATION_PAYER_UNREACHABLE", description = "The error code indicating that the system wasn't able to communicate with the payer during the authorisation process.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_AUTHORIZATION_PAYER_UNREACHABLE = 2542;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_WEEKLY_LIMIT_COUNT", description = "The error code indicating that the payee's wallet weekly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_WEEKLY_LIMIT_COUNT = 2543;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_WEEKLY_LIMIT_COUNT", description = "The error code indicating that the payer's wallet weekly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_WEEKLY_LIMIT_COUNT = 2544;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_MONTHLY_LIMIT_COUNT", description = "The error code indicating that the payee's wallet monthly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_MONTHLY_LIMIT_COUNT = 2545;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_MONTHLY_LIMIT_COUNT", description = "The error code indicating that the payer's wallet monthly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_MONTHLY_LIMIT_COUNT = 2546;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_ABSOLUTE_LIMIT_COUNT", description = "The error code indicating that the payer's wallet absolute limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_ABSOLUTE_LIMIT_COUNT = 2547;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_DAILY_LIMIT_COUNT", description = "The error code indicating that the payee's wallet daily limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_DAILY_LIMIT_COUNT = 2548;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_DAILY_LIMIT_COUNT", description = "The error code indicating that the payer's wallet daily limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_DAILY_LIMIT_COUNT = 2549;

	@MobiliserErrorCodeDescription(name = "ERROR_SUBSCRIPTION_UNKNOWN", description = "The error code indicating that the subscription is unknown.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_SUBSCRIPTION_UNKNOWN = 2550;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_SINGLE_MINIMUM_UNDERRUN", description = "The error code indicating that the payee's wallet single transaction minimum underrun is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_SINGLE_MINIMUM_UNDERRUN = 2551;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_SINGLE_MINIMUM_UNDERRUN", description = "The error code indicating that the payer's wallet single transaction minimum underrun is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_SINGLE_MINIMUM_UNDERRUN = 2552;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WEEKLY_LIMIT_COUNT", description = "The error code indicating that the payee's weekly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WEEKLY_LIMIT_COUNT = 2553;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WEEKLY_LIMIT_COUNT", description = "The error code indicating that the payer's weekly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WEEKLY_LIMIT_COUNT = 2554;

	@MobiliserErrorCodeDescription(name = "ERROR_NO_ACTIVATION_TRANSACTION", description = "The error code indicating that there is no activation transaction for the PI tried to activate.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_NO_ACTIVATION_TRANSACTION = 2555;

	@MobiliserErrorCodeDescription(name = "ERROR_ACTIVATION_AMOUNT_WRONG", description = "The error code indicating that the activation amounts were wrong (at least one of them).", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_ACTIVATION_AMOUNT_WRONG = 2556;

	@MobiliserErrorCodeDescription(name = "ERROR_ACTIVATION_FAILED_PERMANENTLY", description = "The error code indicating that the activation amounts were wrong (at least one of them) and the activation transaction is invalidated.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_ACTIVATION_FAILED_PERMANENTLY = 2557;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_ABSOLUTE_LIMIT_COUNT", description = "The error code indicating that the payee's absolute limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_ABSOLUTE_LIMIT_COUNT = 2559;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_ABSOLUTE_LIMIT_COUNT", description = "The error code indicating that the payer's absolute limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_ABSOLUTE_LIMIT_COUNT = 2560;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_ABSOLUTE_LIMIT", description = "The error code indicating that the payee's absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_ABSOLUTE_LIMIT = 2561;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_ABSOLUTE_LIMIT", description = "The error code indicating that the payer's absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_ABSOLUTE_LIMIT = 2562;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_MONTHLY_LIMIT", description = "The error code indicating that the payee's monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_MONTHLY_LIMIT = 2563;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_MONTHLY_LIMIT", description = "The error code indicating that the payer's monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_MONTHLY_LIMIT = 2564;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_DAILY_LIMIT", description = "The error code indicating that the payee's daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_DAILY_LIMIT = 2565;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_DAILY_LIMIT", description = "The error code indicating that the payer's daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_DAILY_LIMIT = 2566;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WEEKLY_LIMIT", description = "The error code indicating that the payee's weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WEEKLY_LIMIT = 2567;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WEEKLY_LIMIT", description = "The error code indicating that the payer's weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WEEKLY_LIMIT = 2568;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_SINGLE_LIMIT", description = "The error code indicating that the payee's single transaction limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_SINGLE_LIMIT = 2569;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_SINGLE_LIMIT", description = "The error code indicating that the payer's single transaction limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_SINGLE_LIMIT = 2570;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_ABSOLUTE_LIMIT", description = "The error code indicating that the payee's wallet absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_ABSOLUTE_LIMIT = 2571;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_ABSOLUTE_LIMIT", description = "The error code indicating that the payer's wallet absolute limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_ABSOLUTE_LIMIT = 2572;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_MONTHLY_LIMIT", description = "The error code indicating that the payee's wallet monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_MONTHLY_LIMIT = 2573;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_MONTHLY_LIMIT", description = "The error code indicating that the payer's wallet monthly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_MONTHLY_LIMIT = 2574;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_DAILY_LIMIT", description = "The error code indicating that the payee's wallet daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_DAILY_LIMIT = 2575;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_DAILY_LIMIT", description = "The error code indicating that the payer's wallet daily limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_DAILY_LIMIT = 2576;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_WEEKLY_LIMIT", description = "The error code indicating that the payee's wallet weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_WEEKLY_LIMIT = 2577;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_WEEKLY_LIMIT", description = "The error code indicating that the payer's wallet weekly limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_WEEKLY_LIMIT = 2578;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_WALLET_SINGLE_LIMIT", description = "The error code indicating that the payee's wallet single limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_WALLET_SINGLE_LIMIT = 2579;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_WALLET_SINGLE_LIMIT", description = "The error code indicating that the payer's wallet single limit is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_WALLET_SINGLE_LIMIT = 2580;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYEE_AMOUNT", description = "The error code indicating that there is a transaction restriction for the payee concerning the transaction amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYEE_AMOUNT = 2581;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYER_AMOUNT", description = "The error code indicating that there is a transaction restriction for the payer concerning the transaction amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYER_AMOUNT = 2582;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYEE_COUNT", description = "The error code indicating that there is a transaction restriction for the payee concerning the number of transactions.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYEE_COUNT = 2583;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYER_COUNT", description = "The error code indicating that there is a transaction restriction for the payer concerning the number of transactions.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYER_COUNT = 2584;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYEE_SUM", description = "The error code indicating that there is a transaction restriction for the payee concerning the sum of transaction amounts.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYEE_SUM = 2585;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYER_SUM", description = "The error code indicating that there is a transaction restriction for the payer concerning the sum of transaction amounts.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYER_SUM = 2586;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYEE_AMOUNT_MIN", description = "The error code indicating that there is a transaction restriction for the payee concerning the transaction amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYEE_AMOUNT_MIN = 2587;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_PAYER_AMOUNT_MIN", description = "The error code indicating that there is a transaction restriction for the payer concerning the transaction amount.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_PAYER_AMOUNT_MIN = 2588;

	@MobiliserErrorCodeDescription(name = "ERROR_RESTRICTION_INDIVIDUAL", description = "The error code indicating that there is an individual restriction for this txn type & customers.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_RESTRICTION_INDIVIDUAL = 2589;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_KYC_INSUFFICIENT", description = "The error code indicating that the payer has insufficient KYC level", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_KYC_INSUFFICIENT = 2590;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_KYC_INSUFFICIENT", description = "The error code indicating that the payee has insufficient KYC level", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_KYC_INSUFFICIENT = 2591;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_SINGLE_MINIMUM_UNDERRUN", description = "The error code indicating that the payee's single transaction minimum underrun is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_SINGLE_MINIMUM_UNDERRUN = 2592;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_SINGLE_MINIMUM_UNDERRUN", description = "The error code indicating that the payer's single transaction minimum underrun is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_SINGLE_MINIMUM_UNDERRUN = 2593;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_MONTHLY_LIMIT_COUNT", description = "The error code indicating that the payee's monthly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_MONTHLY_LIMIT_COUNT = 2594;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_MONTHLY_LIMIT_COUNT", description = "The error code indicating that the payer's monthly limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_MONTHLY_LIMIT_COUNT = 2595;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYEE_DAILY_LIMIT_COUNT", description = "The error code indicating that the payee's daily limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYEE_DAILY_LIMIT_COUNT = 2596;

	@MobiliserErrorCodeDescription(name = "ERROR_PAYER_DAILY_LIMIT_COUNT", description = "The error code indicating that the payer's daily limit count is hit.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_PAYER_DAILY_LIMIT_COUNT = 2597;

	@MobiliserErrorCodeDescription(name = "ERROR_TRANSACTION_PERMANENT", description = "The error code indicating that transaction processing requires manual intervention.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_TRANSACTION_PERMANENT = 2599;

	private static final int ERROR_GROUP_IVR = 2800;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_CONNECT", description = "The error code indicating that connecting to the IVR failed.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_CONNECT = 2801;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_COMMUNICATION", description = "The error code indicating that an IVR service timeout occurred.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_COMMUNICATION = 2802;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_SERVICE_TIMEOUT", description = "The error code indicating that an IVR service timeout occurred.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_SERVICE_TIMEOUT = 2803;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_NO_USER_INPUT", description = "The error code indicating that the IVR user did not gave any input", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_NO_USER_INPUT = 2804;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_NO_PICKUP", description = "The error code indicating that a user did not pickup the phone.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_NO_PICKUP = 2805;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_BUSY", description = "The error code indicating that a phone was busy.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_BUSY = 2806;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_HANGUP", description = "The error code indicating that a user ended an IVR call by hanging up.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_HANGUP = 2811;

	@MobiliserErrorCodeDescription(name = "ERROR_IVR_UNKNOWN", description = "The error code indicating that an unexpected IVR error occurred.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_IVR_UNKNOWN = 2899;
	private static final int ERROR_GROUP_WORKFLOW = 2900;

	@MobiliserErrorCodeDescription(name = "ERROR_WORKFLOW_FAILED", description = "Indicates that a task in a workflow definition failed whie executing.", infoLevel = MobiliserErrorCodeDescription.InfoMode.ERROR)
	public static final int ERROR_WORKFLOW_FAILED = 2901;

	@MobiliserErrorCodeDescription(name = "ERROR_WORKFLOW_REJECTED", description = "Indicates that a task in a workflow definition was rejected.", infoLevel = MobiliserErrorCodeDescription.InfoMode.ERROR)
	public static final int ERROR_WORKFLOW_REJECTED = 2902;
	private static final int ERROR_GROUP_MISC = 9900;

	@MobiliserErrorCodeDescription(name = "ERROR_DB", description = "Indicates that an unexpected database error occured.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_DB = 9935;

	@MobiliserErrorCodeDescription(name = "ERROR_UNKNOWN", description = "Indicates that an unexpected error occured.", infoLevel = MobiliserErrorCodeDescription.InfoMode.INFO)
	public static final int ERROR_UNKNOWN = 9999;
}
