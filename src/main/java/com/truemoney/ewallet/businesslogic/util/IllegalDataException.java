package com.truemoney.ewallet.businesslogic.util;

import java.util.Collections;

import vn.mog.framework.service.api.MobiliserServiceException;

public class IllegalDataException extends MobiliserServiceException {
	private static final String ILLEGAL_DATA = "ILLEGAL_DATA";
	private static final long serialVersionUID = 1L;

	public IllegalDataException() {
	}

	public IllegalDataException(String message, String illegalData) {
		super(message, Collections.singletonMap(ILLEGAL_DATA, illegalData));
	}

	public IllegalDataException(Throwable cause, String illegalData) {
		super(cause, Collections.singletonMap(ILLEGAL_DATA, illegalData));
	}

	public IllegalDataException(String message, Throwable cause, String illegalData) {
		super(message, cause, Collections.singletonMap(ILLEGAL_DATA, illegalData));
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_GENERIC_ILLEGAL_DATA;
	}
}
