package com.truemoney.ewallet.businesslogic.util;

import vn.mog.framework.service.api.MobiliserServiceException;

public class AccessDeniedMobiliserServiceException extends MobiliserServiceException {
	private static final long serialVersionUID = 1L;

	public AccessDeniedMobiliserServiceException() {
	}

	public AccessDeniedMobiliserServiceException(String message) {
		super(message);
	}

	public AccessDeniedMobiliserServiceException(Throwable cause) {
		super(cause);
	}

	public AccessDeniedMobiliserServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_GENERIC_ACCESS_DENIED;
	}
}
