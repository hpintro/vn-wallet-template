package com.truemoney.ewallet.businesslogic.util;

import java.util.Collections;

import vn.mog.framework.service.api.MobiliserServiceException;

public class IllegalRequestException extends MobiliserServiceException {
	private static final String ILLEGAL_FIELD = "ILLEGAL_FIELD";
	private static final long serialVersionUID = 1L;

	public IllegalRequestException() {
	}

	public IllegalRequestException(String message, String illegalData) {
		super(message, Collections.singletonMap(ILLEGAL_FIELD, illegalData));
	}

	public IllegalRequestException(Throwable cause, String illegalData) {
		super(cause, Collections.singletonMap(ILLEGAL_FIELD, illegalData));
	}

	public IllegalRequestException(String message, Throwable cause, String illegalData) {
		super(message, cause, Collections.singletonMap(ILLEGAL_FIELD, illegalData));
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_REQUEST_INVALID;
	}
}
