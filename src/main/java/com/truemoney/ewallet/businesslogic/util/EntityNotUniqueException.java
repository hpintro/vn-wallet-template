package com.truemoney.ewallet.businesslogic.util;

import vn.mog.framework.service.api.MobiliserServiceException;

public class EntityNotUniqueException extends MobiliserServiceException {
	private static final long serialVersionUID = 1L;

	public EntityNotUniqueException() {
	}

	public EntityNotUniqueException(String message) {
		super(message);
	}

	public EntityNotUniqueException(Throwable cause) {
		super(cause);
	}

	public EntityNotUniqueException(String message, Throwable cause) {
		super(message, cause);
	}

	public int getErrorCode() {
		return StatusCodes.ERROR_GENERIC_ENTITY_NOT_UNIQUE;
	}
}
