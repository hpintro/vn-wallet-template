package com.truemoney.ewallet.businesslogic.util;

public class IdentificationTypes {
	public static final int IDENTIFICATION_TYPE_MSISDN = 0;
	public static final int IDENTIFICATION_TYPE_CUSTOMER_ID = 1;
	public static final int IDENTIFICATION_TYPE_IMEI_MOBILE = 2;
	public static final int IDENTIFICATION_TYPE_IMEI_CO_USER = 3;
	public static final int IDENTIFICATION_TYPE_FOREIGN_CUSTOMER_ID = 4;
	public static final int IDENTIFICATION_TYPE_USER_NAME = 5;
	public static final int IDENTIFICATION_TYPE_CARD_NUMBER = 6;
	public static final int IDENTIFICATION_TYPE_EMAIL = 7;
}
