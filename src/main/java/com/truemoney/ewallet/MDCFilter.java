package com.truemoney.ewallet;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.MDC;
import org.springframework.web.filter.GenericFilterBean;

import com.truemoney.ewallet.common.util.WebUtils;

public class MDCFilter extends GenericFilterBean {
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		String ip = WebUtils.getClientIp(req);

		MDC.put("IP", ip);
		try {
			chain.doFilter(req, resp);
		} finally {
			MDC.remove("IP");
		}
	}

}
