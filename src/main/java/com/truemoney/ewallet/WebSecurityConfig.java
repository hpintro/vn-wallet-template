package com.truemoney.ewallet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import com.truemoney.ewallet.security.jwt.JwtAuthFilter;
import com.truemoney.ewallet.security.jwt.JwtAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthEndPoint;

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
				// we don't need CSRF because our token is invulnerable
				.anonymous().disable()
				.csrf().disable()
				
				.exceptionHandling().authenticationEntryPoint(jwtAuthEndPoint).and()

				// don't create session
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

				.authorizeRequests()
				//.authorizeRequests()
                .anyRequest().access("hasRole('USERSS')")
                .antMatchers("/api/v1/test/hello").permitAll()
                .antMatchers("/api/v1/present/**").permitAll()
				.anyRequest().authenticated();
				// .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
//				.antMatchers("/api/**").access("hasRole('XXXXXXXX')")
//				// allow anonymous resource requests
//				.antMatchers(HttpMethod.GET, "/", "/*.html", "/favicon.ico", "/**/*.html", "/**/*.css", "/**/*.js").permitAll()
//				.antMatchers("/auth/**").permitAll().antMatchers("/api/service/requestTransaction").permitAll()
//				.antMatchers("/api/service/getBalance").permitAll()
//				.anyRequest().authenticated();

		// Custom JWT based security filter
		httpSecurity.addFilterBefore(new MDCFilter(), UsernamePasswordAuthenticationFilter.class);
		//httpSecurity.addFilterBefore(new CustomTokenAuthenticationFilter("/**"), UsernamePasswordAuthenticationFilter.class);
		//httpSecurity.addFilterBefore(new JwtAuthFilter(), UsernamePasswordAuthenticationFilter.class);

		//httpSecurity.addFilterAfter(new CustomOAuth2AuthenticationProcessingFilter(), AbstractPreAuthenticatedProcessingFilter.class);
		
		// disable page caching
		httpSecurity.headers().cacheControl();
	}
	
}
