package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.ResponseType;

public class LogoutResponse extends ResponseType implements Serializable {
	protected boolean signedOut;

	public boolean isSignedOut() {
		return signedOut;
	}

	public void setSignedOut(boolean signedOut) {
		this.signedOut = signedOut;
	}

}
