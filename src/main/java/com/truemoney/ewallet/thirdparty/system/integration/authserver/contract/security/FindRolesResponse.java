package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;
import java.util.Collection;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.ResponseType;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.Role;

public class FindRolesResponse extends ResponseType implements Serializable {
	protected Collection<Role> roles;
	protected Long total;

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}
}
