package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.ResponseType;

public class ResetPasswordResponse extends ResponseType implements Serializable {
	protected boolean updated;

	public boolean isUpdated() {
		return updated;
	}

	public void setUpdated(boolean updated) {
		this.updated = updated;
	}

}
