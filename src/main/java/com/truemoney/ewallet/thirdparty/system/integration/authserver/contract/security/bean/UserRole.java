package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean;

import java.io.Serializable;
import java.util.Date;

public class UserRole implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long userId;
	private Long roleId;
	private Boolean active;

	protected Long creator;
	protected Date createdTime;
	protected Long lastUpdater;
	protected Date lastUpdatedTime;

	public UserRole(Long id, Long userId, Long roleId, Boolean active, Long creator, Date createdTime, Long lastUpdater,
			Date lastUpdatedTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.roleId = roleId;
		this.active = active;
		this.creator = creator;
		this.createdTime = createdTime;
		this.lastUpdater = lastUpdater;
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public UserRole() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getActive() {
		return active;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Long getLastUpdater() {
		return lastUpdater;
	}

	public void setLastUpdater(Long lastUpdater) {
		this.lastUpdater = lastUpdater;
	}

	public Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
}
