package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;
import java.util.List;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.ResponseType;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.User;

public class FindPermissionListResponse extends ResponseType implements Serializable {
	protected List<User> users;

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
