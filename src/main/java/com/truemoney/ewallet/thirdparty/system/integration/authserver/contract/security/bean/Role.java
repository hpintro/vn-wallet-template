package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class Role implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String description;
	private String type;

	private Boolean active;
	private Long creator;
	private Date createdTime;
	private Long lastUpdater;
	private Date lastUpdatedTime;

	private Collection<Permission> permissions;

	public Role() {
	}

	public Role(Long id, String name, String description, String type, Boolean active, Long creator, Date createdTime,
			Long lastUpdater, Date lastUpdatedTime, Collection<Permission> permissions) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.type = type;
		this.active = active;
		this.creator = creator;
		this.createdTime = createdTime;
		this.lastUpdater = lastUpdater;
		this.lastUpdatedTime = lastUpdatedTime;
		this.permissions = permissions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Long getLastUpdater() {
		return lastUpdater;
	}

	public void setLastUpdater(Long lastUpdater) {
		this.lastUpdater = lastUpdater;
	}

	public Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Collection<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Collection<Permission> permissions) {
		this.permissions = permissions;
	}

}
