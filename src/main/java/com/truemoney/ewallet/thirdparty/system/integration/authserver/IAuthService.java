package com.truemoney.ewallet.thirdparty.system.integration.authserver;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.CreateUserRequest;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.CreateUserResponse;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.GetUserRequest;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.GetUserResponse;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.UpdateUserRequest;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.UpdateUserResponse;

public interface IAuthService {
	void setAccessToken(String accessToken);

	public GetUserResponse getUser(GetUserRequest request) throws Exception;

	CreateUserResponse createUser(CreateUserRequest request) throws Exception;

	UpdateUserResponse updateUser(UpdateUserRequest request) throws Exception;

}
