package com.truemoney.ewallet.thirdparty.system.integration;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.truemoney.ewallet.SharedConstants;
import com.truemoney.ewallet.common.util.Utils;

@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class AbstractAPIClient {
	private final static Logger logger = Logger.getLogger(AbstractAPIClient.class);
	// --------------------------------
	public static final String AUTHORIZATION = "Authorization";
	public static final String BEARER_HEADER_PREFIX = "Bearer ";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String USER_AGENT = "User-Agent";
	// --------------------------------
	@Autowired
	@Qualifier("apiRestTemplate")
	protected RestTemplate restTemplate;
	// --------------------------------
	public static MultiThreadedHttpConnectionManager httpConnectionManager;
	public static HttpClient httpClient;
	static {

		if (httpConnectionManager == null) {
			httpConnectionManager = new MultiThreadedHttpConnectionManager();
			HttpConnectionManagerParams params = new HttpConnectionManagerParams();
			params.setDefaultMaxConnectionsPerHost(100);
			params.setMaxTotalConnections(5000);
			params.setParameter(HttpConnectionManagerParams.CONNECTION_TIMEOUT,
					SharedConstants.HTTP_CLIENT_CONNECT_TIMEOUT);
			params.setParameter(HttpConnectionManagerParams.SO_TIMEOUT, SharedConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
			httpConnectionManager.setParams(params);
		}
		if (httpClient == null)
			httpClient = new HttpClient(httpConnectionManager);
	}

	// --------------------------------
	protected abstract String getClientID();

	protected abstract String getBaseRequestURL();

	protected abstract String getBearerHeaderPrefix();

	protected abstract boolean isSessionStorage();

	// --------------------------------
	private String accessToken;

	public String getAccessToken() {
		if (isSessionStorage()) {
			try {
				if (getCurrentHttpServletRequest() != null)
					this.accessToken = (String) getCurrentHttpServletRequest().getSession()
							.getAttribute(getClientID() + "_access_token");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (this.accessToken != null && this.accessToken.startsWith(getBearerHeaderPrefix()))
			this.accessToken = this.accessToken.substring(getBearerHeaderPrefix().length());
		return this.accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
		if (this.accessToken != null && this.accessToken.startsWith(getBearerHeaderPrefix()))
			this.accessToken = this.accessToken.substring(getBearerHeaderPrefix().length());
		if (isSessionStorage()) {
			try {
				if (getCurrentHttpServletRequest() != null)
					getCurrentHttpServletRequest().getSession().setAttribute(getClientID() + "_access_token",
							this.accessToken);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
		}
	}

	// --------------------------------
	public <T> T callRequest(String requestURI, Object request, Class<T> clazz) throws Exception {
		Map<String, String> mapHeader = new HashMap<String, String>();
		if (StringUtils.isNotBlank(getAccessToken()))
			mapHeader.put(AUTHORIZATION, getBearerHeaderPrefix() + getAccessToken());
		return callRequest(requestURI, mapHeader, request, clazz);
	}

	public <T> T callRequest(String requestURI, Map<String, String> mapHeader, Object request, Class<T> clazz)
			throws Exception {
		try {
			final String url = getBaseRequestURL() + requestURI;
			// --------------
			logger.info(getClientID().toUpperCase() + " URL: " + url);
			logger.info("REQ: " + Utils.objectToJson(request));
			// --------------
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			if (mapHeader != null && !mapHeader.isEmpty()) {
				Iterator<Entry<String, String>> iterator = mapHeader.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<String, String> entry = iterator.next();
					headers.add(entry.getKey(), entry.getValue());
					logger.info(entry.getKey() + ":    " + entry.getValue());
				}
			}
			final HttpEntity walletRequest = new HttpEntity(request, headers);
			// --------------
			T resp = restTemplate.postForObject(url, walletRequest, clazz);
			if (resp == null)
				throw new NullPointerException();
			logger.info("RESP:    " + Utils.objectToJson(resp));
			return resp;
		} catch (HttpStatusCodeException ex) {
			int statusCode = ex.getStatusCode().value();
			logger.error("RESPONE HTTP CODE: " + statusCode);
			logger.error(ex.getMessage(), ex);
			if (statusCode == 401)
				throw new SecurityException("Not Authorized!");
			throw ex;
		} catch (RestClientException ex) {
			// Server not found
			logger.error(ex.getMessage(), ex);
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			throw ex;
		}
	}

	public HttpServletRequest getCurrentHttpServletRequest() {
		try {
			ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpServletRequest request = sra.getRequest();
			return request;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
