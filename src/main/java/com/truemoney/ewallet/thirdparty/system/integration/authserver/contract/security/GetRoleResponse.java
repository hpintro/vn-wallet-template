package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.ResponseType;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.Role;

public class GetRoleResponse extends ResponseType implements Serializable {
	protected Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
