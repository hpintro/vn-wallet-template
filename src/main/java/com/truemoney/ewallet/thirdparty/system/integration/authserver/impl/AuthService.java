package com.truemoney.ewallet.thirdparty.system.integration.authserver.impl;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.IAuthService;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.CreateUserRequest;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.CreateUserResponse;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.GetUserRequest;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.GetUserResponse;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.UpdateUserRequest;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.UpdateUserResponse;

@Component
public class AuthService implements IAuthService, InitializingBean {
	@Autowired
	private AuthServerAPIClient authServerAPIClient;

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		if (this.authServerAPIClient == null)
			throw new IllegalStateException("authServerAPIClient is required");
	}

	@Override
	public void setAccessToken(String accessToken) {
		authServerAPIClient.setAccessToken(accessToken);
	}

	public GetUserResponse getUser(GetUserRequest request) throws Exception {
		String requestURI = "/api/user/getUser";

		return authServerAPIClient.callRequest(requestURI, request, GetUserResponse.class);
	}

	@Override
	public CreateUserResponse createUser(CreateUserRequest request) throws Exception {
		String requestURI = "/api/user/createUser";

		return authServerAPIClient.callRequest(requestURI, request, CreateUserResponse.class);
	}

	@Override
	public UpdateUserResponse updateUser(UpdateUserRequest request) throws Exception {
		String requestURI = "/api/user/updateUser";

		return authServerAPIClient.callRequest(requestURI, request, UpdateUserResponse.class);
	}
}
