package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;
import java.util.Collection;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.ResponseType;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.Permission;

public class FindPermissionsResponse extends ResponseType implements Serializable {
	protected Collection<Permission> permissions;
	protected Long total;

	public Collection<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Collection<Permission> permissions) {
		this.permissions = permissions;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

}
