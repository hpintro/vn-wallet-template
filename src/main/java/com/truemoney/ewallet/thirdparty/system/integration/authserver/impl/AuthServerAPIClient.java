package com.truemoney.ewallet.thirdparty.system.integration.authserver.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.truemoney.ewallet.SharedConstants;
import com.truemoney.ewallet.common.AuthToken;
import com.truemoney.ewallet.thirdparty.system.integration.AbstractAPIClient;

@Service
public class AuthServerAPIClient extends AbstractAPIClient {
	private static Logger logger = Logger.getLogger(AuthServerAPIClient.class);

	// --------------------------------
	@Override
	protected String getClientID() {
		// TODO Auto-generated method stub
		return "authserver";
	}

	@Override
	protected String getBaseRequestURL() {
		// TODO Auto-generated method stub
		return SharedConstants.AUTH_SERVER_BASE_REQUEST_URL;
	}

	@Override
	protected String getBearerHeaderPrefix() {
		// TODO Auto-generated method stub
		return BEARER_HEADER_PREFIX;
	}

	@Override
	protected boolean isSessionStorage() {
		// TODO Auto-generated method stub
		return true;
	}

	// --------------------------------
	public void authToken() {
		final String url = SharedConstants.AUTH_SERVER_BASE_REQUEST_URL + "/api/auth/token";
		HttpServletRequest httpServletRequest = getCurrentHttpServletRequest();

		// Param
		String username = "user";
		String password = "password";
		String grantType = "password";
		String scope = "read write";
		String clientSecret = "123456";
		String clientId = "clientapp";
		String basicAuthen = "clientapp:123456";

		// request.
		// -----------
		final PostMethod method = new PostMethod(url);
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new NameValuePair("username", username));
			urlParameters.add(new NameValuePair("password", password));
			urlParameters.add(new NameValuePair("grant_type", grantType));
			urlParameters.add(new NameValuePair("scope", scope));
			urlParameters.add(new NameValuePair("client_secret", clientSecret));
			urlParameters.add(new NameValuePair("client_id", clientId));

			method.addParameters(urlParameters.toArray(new NameValuePair[urlParameters.size()]));

			String encoding = DatatypeConverter.printBase64Binary(basicAuthen.getBytes("UTF-8"));
			method.setRequestHeader("Authorization", "Basic " + encoding);
			if (httpServletRequest != null)
				method.setRequestHeader(USER_AGENT, httpServletRequest.getHeader(USER_AGENT));

			int responseCode = httpClient.executeMethod(method);
			logger.info("ResponseCode: " + responseCode);

			String json = method.getResponseBodyAsString();
			if (json != null) {
				AuthToken authToken = new ObjectMapper().readValue(json, AuthToken.class);

				// Login fail: invalid_grant
				if (StringUtils.isBlank(authToken.getAccessToken()))
					return;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}
	}
}
