package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.RequestType;

public class GetRoleRequest extends RequestType implements Serializable {
	protected Long roleId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}
