package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.RequestType;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.Permission;

public class CreatePermissionRequest extends RequestType implements Serializable {

	protected Permission permission;

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}
}
