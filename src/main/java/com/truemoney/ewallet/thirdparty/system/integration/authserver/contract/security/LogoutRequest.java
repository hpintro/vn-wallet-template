package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.RequestType;

public class LogoutRequest extends RequestType implements Serializable {
	private String test = "Bye bye Server";

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
}
