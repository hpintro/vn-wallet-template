package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.RequestType;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.Role;

public class UpdateRoleRequest extends RequestType implements Serializable {

	protected Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
