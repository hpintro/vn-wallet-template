package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String cif;
	private String type;
	private String username;
	private String password;

	private Boolean active;
	private Long creator;
	private Date createdTime;
	private Long lastUpdater;
	private Date lastUpdatedTime;

	private Collection<Role> roles;

	public User() {
	}

	public User(Long id, String cif, String type, String username, Boolean active, Long creator, Date createdTime,
			Long lastUpdater, Date lastUpdatedTime, Collection<Role> roles) {
		super();
		this.id = id;
		this.cif = cif;
		this.type = type;
		this.username = username;
		this.active = active;
		this.creator = creator;
		this.createdTime = createdTime;
		this.lastUpdater = lastUpdater;
		this.lastUpdatedTime = lastUpdatedTime;
		this.roles = roles;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Long getLastUpdater() {
		return lastUpdater;
	}

	public void setLastUpdater(Long lastUpdater) {
		this.lastUpdater = lastUpdater;
	}

	public Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	@JsonIgnore
	@Transient
	public Collection<Permission> getPermissions() {
		Collection<Role> roles = getRoles();
		Collection<Permission> perms = new HashSet<Permission>();
		if (roles != null && roles.size() > 0)
			for (Role role : roles) {
				if (role.getPermissions() != null && role.getPermissions().size() > 0)
					perms.addAll(role.getPermissions());
			}
		return perms;
	}

	@JsonIgnore
	public boolean checkRole(String name) {
		Collection<Role> roles = getRoles();
		if (roles != null && roles.size() > 0)
			for (Role role : roles) {
				if (role.getName().equals(name)) {
					return true;
				}
			}
		return false;
	}

}
