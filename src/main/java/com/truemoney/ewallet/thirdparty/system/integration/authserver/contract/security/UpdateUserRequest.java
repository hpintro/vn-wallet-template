package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;
import java.util.List;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.RequestType;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.User;

public class UpdateUserRequest extends RequestType implements Serializable {

	protected User user;
	protected List<Long> roleIds;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Long> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Long> roleIds) {
		this.roleIds = roleIds;
	}
}
