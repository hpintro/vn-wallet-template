package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.RequestType;

public class UpdatePasswordRequest extends RequestType implements Serializable {

	protected Long userId;
	protected String oldPass;
	protected String newPass;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getOldPass() {
		return oldPass;
	}

	public void setOldPass(String oldPass) {
		this.oldPass = oldPass;
	}

	public String getNewPass() {
		return newPass;
	}

	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}

}
