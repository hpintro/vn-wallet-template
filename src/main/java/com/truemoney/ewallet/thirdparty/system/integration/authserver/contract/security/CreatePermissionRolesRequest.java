package com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;
import java.util.List;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.base.RequestType;

public class CreatePermissionRolesRequest extends RequestType implements Serializable {

	protected Long permissionId;
	protected List<Long> roleIds;

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public List<Long> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Long> roleIds) {
		this.roleIds = roleIds;
	}
}
