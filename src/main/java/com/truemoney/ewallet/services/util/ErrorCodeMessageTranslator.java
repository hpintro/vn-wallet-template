package com.truemoney.ewallet.services.util;

import java.util.Map;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import vn.mog.framework.service.api.IMobiliserExceptionTranslator;

@Component
@Transactional
public final class ErrorCodeMessageTranslator implements MethodInterceptor {
	private volatile Map<Integer, String> errorCodes;

	private String getErrorMessage(int errorCode) {
		return "Got Error!";
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object result = invocation.proceed();

		if (!(result instanceof IMobiliserExceptionTranslator.TranslationResult)) {
			return result;
		}

		IMobiliserExceptionTranslator.TranslationResult tsResult = (IMobiliserExceptionTranslator.TranslationResult) result;

		if (StringUtils.isBlank(tsResult.message)) {
			return new IMobiliserExceptionTranslator.TranslationResult(getErrorMessage(tsResult.returnCode),
					tsResult.returnCode);
		}

		return tsResult;
	}
}
