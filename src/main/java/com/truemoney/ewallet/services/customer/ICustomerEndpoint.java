package com.truemoney.ewallet.services.customer;

import com.truemoney.ewallet.contract.customer.FindCustomerRequest;
import com.truemoney.ewallet.contract.customer.FindCustomerResponse;

public interface ICustomerEndpoint {
	FindCustomerResponse listCustomers(FindCustomerRequest request);
}
