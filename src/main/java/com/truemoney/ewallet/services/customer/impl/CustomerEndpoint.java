package com.truemoney.ewallet.services.customer.impl;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.truemoney.ewallet.businesslogic.customer.ICustomerLogic;
import com.truemoney.ewallet.businesslogic.exception.ErrorConstants;
import com.truemoney.ewallet.contract.customer.FindCustomerRequest;
import com.truemoney.ewallet.contract.customer.FindCustomerResponse;
import com.truemoney.ewallet.converter.customer.ICustomerConverter;
import com.truemoney.ewallet.services.customer.ICustomerEndpoint;

import vn.mog.framework.contract.base.MobiliserResponseType.Status;
import vn.mog.framework.security.api.ICallerUtils;

@Component
public class CustomerEndpoint implements ICustomerEndpoint, InitializingBean {

	private static Logger logger = Logger.getLogger(CustomerEndpoint.class);

	@Autowired
	private ICustomerLogic customerLogic;

	@Autowired
	private ICustomerConverter customerConverter;

	@Autowired
	private ICallerUtils callerUtils;

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		if (this.customerLogic == null)
			throw new IllegalStateException("customerLogic is required");
		if (this.customerConverter == null)
			throw new IllegalStateException("customerConverter is required");
		if (this.callerUtils == null)
			throw new IllegalStateException("callerUtils is required");
	}

	@Override
	public FindCustomerResponse listCustomers(FindCustomerRequest request) {
		// TODO Auto-generated method stub
		FindCustomerResponse response = new FindCustomerResponse();
		try {
			String search = request.getId();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(new Status(ErrorConstants.ERROR.code, ErrorConstants.ERROR.message));
		}
		return response;
	}
}
