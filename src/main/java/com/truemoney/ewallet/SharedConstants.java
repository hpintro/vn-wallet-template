package com.truemoney.ewallet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SharedConstants {

	// SOME CHARACTERS
	public static final String SYS_NAME = "MYTOPUP";
	public static final String COMMA = ",";
	public static final String COLON = ":";
	public static final String DASH = "-";
	public static final String SEMI_CONLON = ";";
	public static final String VERTICAL_BAR = "|";
	public static final String UNDERSCORE = "_";

	public static String AUTH_SERVER_BASE_REQUEST_URL = "";

	@Value("${param.ref.authserver.base.url}")
	public void setAuthServerBaseUrl(String value) {
		AUTH_SERVER_BASE_REQUEST_URL = value;
	}

	// Email
	public static String EMAIL_FROM = "";

	@Value("${system.email.from}")
	public void setEmailFrom(String value) {
		EMAIL_FROM = value;
	}

	public static String EMAIL_REPLY_TO = "noreply@1pay.vn";

	@Value("${system.email.replyTo}")
	public void setEmailReplyTo(String value) {
		EMAIL_REPLY_TO = value;
	}

	/*
	 * Sms
	 */
	public static String SMS_API_URL = "";

	@Value("${system.sms.api.url}")
	public void setSmsAPIURL(String value) {
		SMS_API_URL = value;
	}

	public static String SMS_API_USER_NAME = "";

	@Value("${system.sms.api.username}")
	public void setSmsAPIUsername(String value) {
		SMS_API_USER_NAME = value;
	}

	public static String SMS_API_PASSWORD = "";

	@Value("${system.sms.api.password}")
	public void setSmsAPIPassword(String value) {
		SMS_API_PASSWORD = value;
	}

	public static String SMS_API_BRAND = "";

	@Value("${system.sms.api.brand}")
	public void setSmsAPIBrand(String value) {
		SMS_API_BRAND = value;
	}

	public static class Role {

		public static final String ROLE_SYSTEM = "ROLE_SYSTEM";

		public static final String ROLE_CUSTOMER = "ROLE_CUSTOMER";

		public static final String ROLE_USER = "ROLE_USER";

		public static final String ROLE_MERCHANT = "ROLE_MERCHANT";

		public static final String ROLE_STAFF = "ROLE_STAFF";

		public static final String ROLE_ADMIN = "ROLE_ADMIN";

		public static final String ROLE_FINANCE = "ROLE_FINANCE";

		public static final String ROLE_SALE = "ROLE_SALE";

		public static final String ROLE_SALE_MANAGER = "ROLE_SALE_MANAGER";

		public static final String ROLE_SALE_SUPPORT = "ROLE_SALE_SUPPORT";

		public static final String ROLE_CUSTOMER_CARE = "ROLE_CUSTOMER_CARE";

	}

	public static int HTTP_CLIENT_CONNECT_TIMEOUT = 5000;// ms
	public static int HTTP_CLIENT_SOCKET_TIMEOUT = 20000;// ms

	public static int SLA_AIRTIME_TIMEOUT = 3000000;// ms
	public static int SLA_PROVIDER_TIMEOUT = HTTP_CLIENT_SOCKET_TIMEOUT + 2000;// ms
}
