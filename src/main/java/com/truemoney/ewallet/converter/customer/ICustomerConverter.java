package com.truemoney.ewallet.converter.customer;

import java.util.Collection;

public interface ICustomerConverter {
	com.truemoney.ewallet.persistence.model.customer.Customer fromContract(
			com.truemoney.ewallet.contract.customer.bean.Customer contract);

	com.truemoney.ewallet.contract.customer.bean.Customer toContract(
			com.truemoney.ewallet.persistence.model.customer.Customer model);

	public Collection<com.truemoney.ewallet.contract.customer.bean.Customer> toContracts(
			Collection<com.truemoney.ewallet.persistence.model.customer.Customer> models);
}
