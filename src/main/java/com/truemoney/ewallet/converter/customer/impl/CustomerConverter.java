package com.truemoney.ewallet.converter.customer.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.truemoney.ewallet.contract.customer.bean.Customer;
import com.truemoney.ewallet.converter.customer.ICustomerConverter;
import com.truemoney.ewallet.persistence.dao.api.factory.DaoFactory;

@Service
@Transactional
public class CustomerConverter implements ICustomerConverter {
	@Autowired
	DaoFactory daoFactory;

	@Override
	public com.truemoney.ewallet.persistence.model.customer.Customer fromContract(Customer contract) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Customer toContract(com.truemoney.ewallet.persistence.model.customer.Customer model) {

		// TODO Auto-generated method stub
		Customer cus = new Customer();

		cus.setId(model.getId());
		cus.setCif(model.getCif());
		cus.setUsername(model.getUsername());
		cus.setEmail(model.getEmail());
		cus.setMsisdn(model.getMsisdn());
		cus.setFullName(model.getFullName());
		return cus;
	}

	@Override
	public Collection<com.truemoney.ewallet.contract.customer.bean.Customer> toContracts(
			Collection<com.truemoney.ewallet.persistence.model.customer.Customer> models) {
		// TODO Auto-generated method stub
		List<Customer> listCustomer = new ArrayList<Customer>();
		if (models != null && !models.isEmpty()) {
			for (com.truemoney.ewallet.persistence.model.customer.Customer model : models) {
				listCustomer.add(toContract(model));
			}
		}
		return listCustomer;
	}

}
