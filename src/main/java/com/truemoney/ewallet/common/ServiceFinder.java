package com.truemoney.ewallet.common;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ServiceFinder {
	private static final Logger logger = Logger.getLogger(ServiceFinder.class);

	private static ApplicationContext ctx = null;

	public static ApplicationContext getContext(HttpServletRequest httpRequest) {
		return ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(httpRequest.getSession().getServletContext());
	}

	public static ApplicationContext getContext(ServletContext context) {
		return ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
	}

	public static ApplicationContext getContext() {
		logger.debug("ApplicationContext =" + ctx);
		return ctx;
	}

	public static void setContext(ApplicationContext applicationContext) {
		logger.debug("ApplicationContext =" + applicationContext);
		ctx = applicationContext;
	}
}
