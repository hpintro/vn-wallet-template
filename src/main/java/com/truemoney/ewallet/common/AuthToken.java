package com.truemoney.ewallet.common;

import java.io.Serializable;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

public class AuthToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

//	 @Override
//	 public String toString() {
//	 // TODO Auto-generated method stub
//	 return new Gson().toJson(this);
//	 }

	@JsonProperty("access_token")
	private String accessToken = "322232";

	@JsonProperty("token_type")
	private String tokenType = "XC";

	@JsonProperty("refresh_token")
	private String refreshToken;

	@JsonProperty("expires_in")
	private long expiresIn;

	@JsonProperty
	private String scope;

	@JsonProperty
	private String error;

	@JsonProperty("error_description")
	private String errorDescription;

	@JsonProperty
	private String result;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
