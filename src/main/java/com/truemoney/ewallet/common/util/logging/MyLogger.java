package com.truemoney.ewallet.common.util.logging;

import java.lang.reflect.Method;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MyLogger extends Logger {
	// Our fully qualified class name.
	static String FQCN = MyLogger.class.getName();

	static boolean JDK14 = false;
	static {
		String version = System.getProperty("java.version");
		if (version != null) {
			JDK14 = version.startsWith("1.4");
		}
	}

	private Logger logger;

	public MyLogger(String name) {
		super(name);
		this.logger = Logger.getLogger(name);
	}

	@SuppressWarnings("rawtypes")
	public MyLogger(Class clazz) {
		this(clazz.getName());
	}

	public void trace(Object msg) {
		logger.log(FQCN, XLevel.TRACE, msg, null);
	}

	public void trace(Object msg, Throwable t) {
		logger.log(FQCN, XLevel.TRACE, msg, t);
		logNestedException(XLevel.TRACE, msg, t);
	}

	public boolean isTraceEnabled() {
		return logger.isEnabledFor(XLevel.TRACE);
	}

	public void debug(Object msg) {
		logger.log(FQCN, Level.DEBUG, msg, null);
	}

	public void debug(Object msg, Throwable t) {
		logger.log(FQCN, Level.DEBUG, msg, t);
		logNestedException(Level.DEBUG, msg, t);
	}

	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	public void info(Object msg) {
		logger.log(FQCN, Level.INFO, msg, null);
	}

	public void info(Object msg, Throwable t) {
		logger.log(FQCN, Level.INFO, msg, t);
		logNestedException(Level.INFO, msg, t);
	}

	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

	public void warn(Object msg) {
		logger.log(FQCN, Level.WARN, msg, null);
	}

	public void warn(Object msg, Throwable t) {
		logger.log(FQCN, Level.WARN, msg, t);
		logNestedException(Level.WARN, msg, t);
	}

	public void error(Object msg) {
		logger.log(FQCN, Level.ERROR, msg, null);
	}

	public void error(Object msg, Throwable t) {
		logger.log(FQCN, Level.ERROR, msg, t);
		logNestedException(Level.ERROR, msg, t);
	}

	public void fatal(Object msg) {
		logger.log(FQCN, Level.FATAL, msg, null);
	}

	public void fatal(Object msg, Throwable t) {
		logger.log(FQCN, Level.FATAL, msg, t);
		logNestedException(Level.FATAL, msg, t);
	}

	@SuppressWarnings("rawtypes")
	void logNestedException(Level level, Object msg, Throwable t) {
		if (t == null)
			return;
		try {
			Class tC = t.getClass();
			Method mA[] = tC.getMethods();
			Method nextThrowableMethod = null;
			for (int i = 0; i < mA.length; i++) {
				if (("getCause".equals(mA[i].getName()) && !JDK14) || "getRootCause".equals(mA[i].getName())
						|| "getNextException".equals(mA[i].getName()) || "getException".equals(mA[i].getName())) {
					// check param types
					Class params[] = mA[i].getParameterTypes();
					if (params == null || params.length == 0) {
						// just found the getter for the nested throwable
						nextThrowableMethod = mA[i];
						break; // no need to search further
					}
				}
			}

			if (nextThrowableMethod != null) {
				// get the nested throwable and log it
				Throwable next = (Throwable) nextThrowableMethod.invoke(t, new Object[0]);
				if (next != null) {
					this.logger.log(FQCN, level, "Previous log CONTINUED", next);
				}
			}
		} catch (Exception e) {
			// do nothing
		}
	}

}
