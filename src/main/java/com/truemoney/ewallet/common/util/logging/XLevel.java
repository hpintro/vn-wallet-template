package com.truemoney.ewallet.common.util.logging;

import org.apache.log4j.Level;

/**
 * The XLevel class extends the Level class by introducing a new
 * level called TRACE. TRACE has a lower level than DEBUG.
 */

public final class XLevel extends Level {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	static public final int TRACE_INT = Level.DEBUG_INT - 1;
	private static String TRACE_STR = "TRACE";
	public static final XLevel TRACE = new XLevel(TRACE_INT, TRACE_STR, 7);

	protected XLevel(int level, String strLevel, int syslogEquiv) {
		super(level, strLevel, syslogEquiv);
	}

	/**
	 * Convert the String argument to a level. If the conversion fails then this
	 * method returns {@link #TRACE}.
	 */
	public static Level toLevel(String sArg) {
		return (Level) toLevel(sArg, XLevel.TRACE);
	}

	/**
	 * Convert the String argument to a level. If the conversion fails, return
	 * the level specified by the second argument, i.e. defaultValue.
	 */
	public static Level toLevel(String sArg, Level defaultValue) {
		if (sArg == null) {
			return defaultValue;
		}
		String stringVal = sArg.toUpperCase();
		if (stringVal.equals(TRACE_STR)) {
			return XLevel.TRACE;
		}
		return Level.toLevel(sArg, defaultValue);
	}

	/**
	 * Convert an integer passed as argument to a level. If the conversion
	 * fails, then this method returns {@link #DEBUG}.
	 */
	public static Level toLevel(int i) throws IllegalArgumentException {
		if (i == TRACE_INT) {
			return XLevel.TRACE;
		} else {
			return Level.toLevel(i);
		}
	}

}