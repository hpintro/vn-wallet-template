package com.truemoney.ewallet.common.util;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.truemoney.ewallet.common.util.logging.MyLogger;

public class WebUtils {
	private static final MyLogger MY_LOGGER = new MyLogger(WebUtils.class);
	
	public static String getClientIp(ServletRequest req) {
		try {
			HttpServletRequest request = (HttpServletRequest)req;
			
			String remoteAddr = "";
			if (request != null) {
				remoteAddr = request.getHeader("X-FORWARDED-FOR");
				if (remoteAddr == null || "".equals(remoteAddr)) {
					remoteAddr = request.getRemoteAddr();
				}
			}

			return remoteAddr;
		} catch (Exception e) {
			MY_LOGGER.error(e.getMessage(), e);
		}
		return null;
	}
}
