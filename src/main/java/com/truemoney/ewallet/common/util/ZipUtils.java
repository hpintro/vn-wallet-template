package com.truemoney.ewallet.common.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.commons.lang3.StringUtils;

public class ZipUtils {

	public static void zipFiles(List<String> filePaths, String password, String targetZipFile) {
		try {
			// Initiate ZipFile object with the path/name of the zip file.
			ZipFile zipFile = new ZipFile(targetZipFile);

			// Build the list of files to be added in the array list
			// Objects of type File have to be added to the ArrayList
			ArrayList<File> filesToAdd = new ArrayList<File>();
			for (String filePath : filePaths) {
				if (new File(filePath).isFile()) {
					filesToAdd.add(new File(filePath));
				}
			}

			// Initiate Zip Parameters which define various properties such
			// as compression method, etc.
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE); // set
																			// compression
																			// method
																			// to
																			// store
																			// compression

			// Set the compression level
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			if (StringUtils.trimToNull(password) != null) {
				// Set the encryption flag to true
				// If this is set to false, then the rest of encryption
				// properties are ignored
				parameters.setEncryptFiles(true);

				// Set the encryption method to Standard Zip Encryption
				parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);

				// Set password
				parameters.setPassword(password);
			}

			// Now add files to the zip file
			// Note: To add a single file, the method addFile can be used
			// Note: If the zip file already exists and if this zip file is a
			// split file
			// then this method throws an exception as Zip Format Specification
			// does not
			// allow updating split zip files
			zipFile.addFiles(filesToAdd, parameters);

		} catch (ZipException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws IOException, InterruptedException {

	}
}
