package com.truemoney.ewallet;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.truemoney.ewallet.common.ServiceFinder;

@SpringBootApplication
@ComponentScan({ "com.truemoney,vn.mog" })
public class StartApplication {
	private static final Logger LOGGER = Logger.getLogger(StartApplication.class);

	@Autowired
	private ApplicationContext ctx;

	public static void main(String[] args) {
		SpringApplication.run(StartApplication.class, args);
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
    public ExecutorService threadPoolTaskExecutor() {
		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				4, 
				40, 
				1, 
				TimeUnit.MINUTES, 
				new ArrayBlockingQueue<Runnable>(100000, true),
				new ThreadPoolExecutor.CallerRunsPolicy());

        return executor;
    }
	
//	@Autowired
//	ExecutorService executorService;
//	
//	@Bean
//	public ExecutorCompletionService executorCompletionService(){
//		return new ExecutorCompletionService(executorService);
//		
//
//	}
	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {
		return new EmbeddedServletContainerCustomizer() {
			@Override
			public void customize(ConfigurableEmbeddedServletContainer container) {
				try {
					ServiceFinder.setContext(ctx);

				} catch (Exception e) {
					LOGGER.error("Can not start configuration. Shutdown application immediately!", e);
					int status = SpringApplication.exit(ctx);
					System.exit(status);
				}
			}
		};
	}

}
