package com.truemoney.ewallet.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.truemoney.ewallet.services.customer.ICustomerEndpoint;

import vn.mog.framework.security.api.ICallerUtils;

public abstract class AbstractRestController {
	final static String AUTHORIZATION = "Authorization";
	// ------- CUSTOMER -------//
	@Autowired
	@Qualifier("customerEndpointProxy")
	ICustomerEndpoint customerEndpoint;

	// ------- SERVICE -------//

	@Autowired
	ICallerUtils callerUtils;
	// ------------------//

}
