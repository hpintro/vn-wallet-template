package com.truemoney.ewallet.api.rest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.truemoney.ewallet.SharedConstants;
import com.truemoney.ewallet.common.AuthToken;
import com.truemoney.ewallet.common.util.logging.MyLogger;

@RestController
@RequestMapping("/api/v1/test")
//@PreAuthorize("isAuthenticated()")
public class DemoRestAPIController extends AbstractRestController {
	private static final Logger logger = Logger.getLogger(DemoRestAPIController.class);

	private static final MyLogger myLogger = new MyLogger(DemoRestAPIController.class);
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	@ResponseBody
	public String requestTransaction(HttpServletRequest httpRequest) {
		return "success";
	}
	
	@RequestMapping(value = "/{action}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@PreAuthorize("hasAnyRole('" + SharedConstants.Role.ROLE_MERCHANT + "','" + SharedConstants.Role.ROLE_USER + "')")
	public String requestTransaction(@PathVariable String action, @RequestBody String request,
			HttpServletRequest httpRequest) {
		// LogManager.getRootLogger().setLevel(Level.INFO);

		//logger.info("Hello Kitty!");
		//MDC.put("AuthToken", new AuthToken().toString());
		//logger.info(new AuthToken());
		//logger.debug(new AuthToken());
		
		myLogger.info(new AuthToken());

		//myLogger.logInfo("xx", new AuthToken());
		
		return "success!";
	}
	
	/*@RequestMapping(value = "/oauth/check_token")
    @ResponseBody
    public Map<String, ?> checkToken(@RequestParam("token") String value) {

        OAuth2AccessToken token = resourceServerTokenServices.readAccessToken(value);
        if (token == null) {
            throw new InvalidTokenException("Token was not recognised");
        }

        if (token.isExpired()) {
            throw new InvalidTokenException("Token has expired");
        }

        OAuth2Authentication authentication = resourceServerTokenServices.loadAuthentication(token.getValue());

        Map<String, ?> response = accessTokenConverter.extractPublicInformation(token, authentication);

        return response;
    }*/

}
