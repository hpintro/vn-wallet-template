package com.truemoney.ewallet.api.rest;

import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.truemoney.ewallet.api.rest.thr.Thread1;
import com.truemoney.ewallet.common.util.logging.MyLogger;
import com.truemoney.ewallet.persistence.dao.api.factory.DaoFactory;
import com.truemoney.ewallet.persistence.model.customer.UserPresent;

@RestController
@RequestMapping("/api/v1/present")
public class PresentRestAPIController extends AbstractRestController {
	private static final MyLogger myLogger = new MyLogger(PresentRestAPIController.class);
	
	@Autowired
	DaoFactory daoFactory;
	
	@RequestMapping(value = "/{presentId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public String create(HttpServletRequest httpRequest, @PathVariable long presentId) {
		for (int i = 0; i < 10; i++) {
			UserPresent userPresent = new UserPresent(presentId);
			daoFactory.getUserPresentDAO().save(userPresent, 0L);
		}
		return "success";
	}
	
	@Autowired
    private ExecutorService taskExecutor;

	private static long receiver = 0;
	@RequestMapping(value = "/redeem/{presentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	//@Transactional//(propagation=Propagation.REQUIRES_NEW, isolation=Isolation.SERIALIZABLE)
	public String redeem(HttpServletRequest httpRequest, @PathVariable long presentId) throws Exception {
		Thread1 th = new Thread1();
		th.setReceiver(receiver++);
		th.setPresentId(presentId);
		
		taskExecutor.execute(th);
		//taskExecutor.shutdown(); 
		while(true) {
			try {
				Thread.sleep(100);
				if(StringUtils.isNotBlank(th.getResult()))
					break;
			}catch (Exception e){
				
			}
		}
		//taskExecutor.awaitTermination(100, TimeUnit.MILLISECONDS);
		System.out.println("a:" + th.getResult());
		return th.getResult();
	}
	
	

}
