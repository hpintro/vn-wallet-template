package com.truemoney.ewallet.api.rest.thr;

import com.truemoney.ewallet.businesslogic.customer.PresentLogic;
import com.truemoney.ewallet.common.ServiceFinder;

public class Thread1 implements Runnable {
	long receiver;
	long presentId;
	
	String result ;
	
	
	public long getReceiver() {
		return receiver;
	}


	public void setReceiver(long receiver) {
		this.receiver = receiver;
	}


	public long getPresentId() {
		return presentId;
	}


	public void setPresentId(long presentId) {
		this.presentId = presentId;
	}


	public String getResult() {
		return result;
	}


	public void setResult(String result) {
		this.result = result;
	}


	@Override
    public void run() {
		PresentLogic presentLogic = ServiceFinder.getContext().getBean("presentLogic", PresentLogic.class);
		result = presentLogic.redeem(receiver, presentId);
		
    }
}
