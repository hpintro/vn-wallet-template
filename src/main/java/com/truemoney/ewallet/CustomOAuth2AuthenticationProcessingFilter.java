package com.truemoney.ewallet;

import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;

public class CustomOAuth2AuthenticationProcessingFilter extends OAuth2AuthenticationProcessingFilter {
	public CustomOAuth2AuthenticationProcessingFilter() {
		setTokenExtractor(new CustomTokenExtractor());
	}
}
