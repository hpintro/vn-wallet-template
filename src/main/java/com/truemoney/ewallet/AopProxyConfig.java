package com.truemoney.ewallet;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.truemoney.ewallet.services.customer.ICustomerEndpoint;

import vn.mog.framework.service.monitor.PerformanceMonitoringAdvice;
import vn.mog.framework.service.response.ResponseCodeAdvice;

@Configuration
public class AopProxyConfig {
	@Autowired
	private ResponseCodeAdvice responseCodeAdvice;
	@Autowired
	private PerformanceMonitoringAdvice performanceMonitoringAdvice;

	// ------- CUSTOMER -------//
	@Autowired
	private ICustomerEndpoint customerEndpoint;

	@Bean(name = "customerEndpointProxy")
	@Primary
	public ICustomerEndpoint customerEndpointProxy() {
		ProxyFactory pf = new ProxyFactory();
		pf.addAdvice(responseCodeAdvice);
		pf.addAdvice(performanceMonitoringAdvice);
		pf.setTarget(customerEndpoint);
		return (ICustomerEndpoint) pf.getProxy();
	};

	// ------- SERVICE -------//
}
