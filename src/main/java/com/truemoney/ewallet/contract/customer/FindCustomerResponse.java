package com.truemoney.ewallet.contract.customer;

import java.io.Serializable;
import java.util.Collection;

import com.truemoney.ewallet.contract.customer.bean.Customer;

import vn.mog.framework.contract.base.MobiliserResponseType;

public class FindCustomerResponse extends MobiliserResponseType implements Serializable {

	private static final long serialVersionUID = 1L;

	protected Collection<Customer> customers;

	public Collection<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}
}
