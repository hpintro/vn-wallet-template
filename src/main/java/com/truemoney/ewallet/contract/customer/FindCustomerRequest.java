package com.truemoney.ewallet.contract.customer;

import java.io.Serializable;

import vn.mog.framework.contract.base.MobiliserRequestType;

public class FindCustomerRequest extends MobiliserRequestType implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private int offset;
	private int limit;

	public FindCustomerRequest() {
	}

	public FindCustomerRequest(String id, int offset, int limit) {
		this.id = id;
		this.offset = offset;
		this.limit = limit;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
