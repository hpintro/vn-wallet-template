package com.truemoney.ewallet;

import java.util.UUID;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer
		implements ServletContextListener, ServletRequestListener {

	@Value("${server.contextPath}")
	private String contextPath;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StartApplication.class);
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.setProperty("projectName", contextPath);

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

	public void requestInitialized(ServletRequestEvent arg0) {

		MDC.put("RequestId", UUID.randomUUID());

	}

	public void requestDestroyed(ServletRequestEvent arg0) {
		MDC.clear();
	}

}
