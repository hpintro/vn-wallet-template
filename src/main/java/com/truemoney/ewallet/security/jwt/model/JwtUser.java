package com.truemoney.ewallet.security.jwt.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import vn.mog.framework.security.api.ICustomerAware;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JwtUser implements UserDetails, ICustomerAware, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long customerId;
	private String customerCif;
	private String customerType;
	private String username;
	private String password;
	private Boolean active;

	private Collection<JwtRole> roles;

	public JwtUser() {
	}

	public JwtUser(Long customerId, String customerCif, String customerType, String username, Boolean active,
			Collection<JwtRole> roles) {
		super();
		this.customerId = customerId;
		this.customerCif = customerCif;
		this.customerType = customerType;
		this.username = username;
		this.active = active;
		this.roles = roles;
	}

	@Override
	public long getCustomerId() {
		// TODO Auto-generated method stub
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	@Override
	public String getCustomerCif() {
		// TODO Auto-generated method stub
		return customerCif;
	}

	public void setCustomerCif(String customerCif) {
		this.customerCif = customerCif;
	}

	@Override
	public String getCustomerType() {
		// TODO Auto-generated method stub
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Collection<JwtRole> getRoles() {
		return roles;
	}

	public void setRoles(Collection<JwtRole> roles) {
		this.roles = roles;
	}

	@JsonIgnore
	@Transient
	public Collection<JwtPermission> getPermissions() {
		Collection<JwtRole> roles = getRoles();
		Collection<JwtPermission> perms = new HashSet<JwtPermission>();
		if (roles != null && roles.size() > 0)
			for (JwtRole role : roles) {
				if (role.getPermissions() != null && role.getPermissions().size() > 0)
					perms.addAll(role.getPermissions());
			}
		return perms;
	}

	@JsonIgnore
	public boolean checkRole(String name) {
		Collection<JwtRole> roles = getRoles();
		if (roles != null && roles.size() > 0)
			for (JwtRole role : roles) {
				if (role.getName().equals(name)) {
					return true;
				}
			}
		return false;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.addAll(getRoles());
		authorities.addAll(getPermissions());
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return active;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return active;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return active;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return active;
	}
}
