package com.truemoney.ewallet.security.jwt;

import java.util.HashSet;
import java.util.Set;

import com.truemoney.ewallet.security.jwt.model.JwtPermission;
import com.truemoney.ewallet.security.jwt.model.JwtRole;
import com.truemoney.ewallet.security.jwt.model.JwtUser;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.Permission;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.Role;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.User;

public final class JwtUserFactory {

	public static JwtUser fromContract(User contract) {
		JwtUser jwtUser = new JwtUser();
		jwtUser.setCustomerId(contract.getId());
		jwtUser.setCustomerCif(contract.getCif());
		jwtUser.setCustomerType(contract.getType());
		jwtUser.setUsername(contract.getUsername());
		jwtUser.setActive(contract.getActive());
		if (contract.getRoles() != null && !contract.getRoles().isEmpty()) {
			Set<JwtRole> jwtRoles = new HashSet<JwtRole>();
			for (Role role : contract.getRoles())
				jwtRoles.add(fromContract(role));
			jwtUser.setRoles(jwtRoles);
		}
		return jwtUser;
	}

	public static JwtRole fromContract(Role contract) {
		JwtRole jwtRole = new JwtRole();
		jwtRole.setName(contract.getName());
		jwtRole.setDescription(contract.getDescription());
		jwtRole.setActive(contract.getActive());
		jwtRole.setType(contract.getType());
		if (contract.getPermissions() != null && !contract.getPermissions().isEmpty()) {
			Set<JwtPermission> jwtPermissions = new HashSet<JwtPermission>();
			for (Permission per : contract.getPermissions())
				jwtPermissions.add(fromContract(per));
			jwtRole.setPermissions(jwtPermissions);
		}
		return jwtRole;
	}

	public static JwtPermission fromContract(Permission contract) {
		JwtPermission jwtPermission = new JwtPermission();
		jwtPermission.setName(contract.getName());
		jwtPermission.setDescription(contract.getDescription());
		jwtPermission.setActive(contract.getActive());
		jwtPermission.setType(contract.getType());
		return jwtPermission;
	}

	public static User toContract(JwtUser model) {
		User user = new User();
		user.setId(model.getCustomerId());
		user.setCif(model.getCustomerCif());
		user.setType(model.getCustomerType());
		user.setUsername(model.getUsername());
		user.setActive(model.getActive());
		if (model.getRoles() != null && !model.getRoles().isEmpty()) {
			Set<Role> roles = new HashSet<Role>();
			for (JwtRole jwtRole : model.getRoles())
				roles.add(toContract(jwtRole));
			user.setRoles(roles);
		}
		return user;
	}

	public static Role toContract(JwtRole model) {
		Role role = new Role();
		role.setName(model.getName());
		role.setDescription(model.getDescription());
		role.setActive(model.getActive());
		role.setType(model.getType());
		if (model.getPermissions() != null && !model.getPermissions().isEmpty()) {
			Set<Permission> permissions = new HashSet<Permission>();
			for (JwtPermission per : model.getPermissions())
				permissions.add(toContract(per));
			role.setPermissions(permissions);
		}
		return role;
	}

	public static Permission toContract(JwtPermission model) {
		Permission permission = new Permission();
		permission.setName(model.getName());
		permission.setDescription(model.getDescription());
		permission.setActive(model.getActive());
		permission.setType(model.getType());
		return permission;
	}

}
