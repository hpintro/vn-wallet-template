package com.truemoney.ewallet.security.jwt;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.GenericFilterBean;

import com.truemoney.ewallet.common.ServiceFinder;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.IAuthService;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.GetUserRequest;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.GetUserResponse;
import com.truemoney.ewallet.thirdparty.system.integration.authserver.contract.security.bean.User;

import vn.mog.framework.security.api.MobiliserWebAuthenticationDetailsSource;

public class JwtAuthFilter extends GenericFilterBean {
	private final static Logger logger = Logger.getLogger(JwtAuthFilter.class);
	static final String AUTHORIZATION = "Authorization";
	static final String BEARER_HEADER_PREFIX = "Bearer ";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// -----------
		IAuthService authService = ServiceFinder.getContext().getBean("authService", IAuthService.class);

		MultiReadHttpServletRequest multiReadRequest = new MultiReadHttpServletRequest((HttpServletRequest) request);
		String requestBody = IOUtils.toString(multiReadRequest.getInputStream());
		// -----------
		try {
			Authentication currentAuthentication = SecurityContextHolder.getContext().getAuthentication();
			if (currentAuthentication != null && !currentAuthentication.isAuthenticated())
				return;
			if (StringUtils.isNotBlank(multiReadRequest.getHeader(AUTHORIZATION))) {
				// Use access_token Key
				String accessToken = multiReadRequest.getHeader(AUTHORIZATION);
				if (accessToken.startsWith(BEARER_HEADER_PREFIX))
					accessToken = accessToken.substring(BEARER_HEADER_PREFIX.length());
				// -----------
				authService.setAccessToken(accessToken);
				// -----------
				try {
					GetUserResponse getUserResponse = authService.getUser(new GetUserRequest());
					if (getUserResponse != null) {
						User user = getUserResponse.getUser();
						if (user != null) {
							// ----------
							UserDetails userDetails = JwtUserFactory.fromContract(user);
							UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
									userDetails, null, userDetails.getAuthorities());
							authentication.setDetails(new MobiliserWebAuthenticationDetailsSource()
									.buildDetails((HttpServletRequest) request));
							logger.info("authenticated user " + user.getUsername() + ", setting security context");
							SecurityContextHolder.getContext().setAuthentication(authentication);
							// ----------
						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			} else {
				// System.out.println("Passed!");
				Set<GrantedAuthority> pers = new HashSet<>();
				GrantedAuthority per = new SimpleGrantedAuthority("ROLE_MERCHANT");
				pers.add(per);

				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("DemoAcc",
						null, pers);
				authentication.setDetails(
						new MobiliserWebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
				// logger.info("authenticated user DemoAcc, setting security
				// context");
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} finally {
			chain.doFilter(multiReadRequest, response);
		}
	}
}
