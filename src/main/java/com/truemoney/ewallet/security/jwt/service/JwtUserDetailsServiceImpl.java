package com.truemoney.ewallet.security.jwt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.truemoney.ewallet.thirdparty.system.integration.authserver.IAuthService;

/**
 * Created by stephan on 20.03.16.
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private IAuthService authService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// try {
		// GetUserRequest request = new GetUserRequest();
		// request.setUsername(username);
		// GetUserResponse response = authService.getUser(request);
		// if (response != null) {
		// User user = response.getUser();
		// if (user != null) {
		//
		// }
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
	}

}
