package com.truemoney.ewallet.security.jwt.model;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class JwtRole implements GrantedAuthority, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	private String type;
	private Boolean active;

	private Collection<JwtPermission> permissions;

	public JwtRole() {
	}

	public JwtRole(Long id, String name, String description, String type, Boolean active,
			Collection<JwtPermission> permissions) {
		super();
		this.name = name;
		this.description = description;
		this.type = type;
		this.active = active;
		this.permissions = permissions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Collection<JwtPermission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Collection<JwtPermission> permissions) {
		this.permissions = permissions;
	}

	@Override
	public String getAuthority() {
		return name;
	};

}
