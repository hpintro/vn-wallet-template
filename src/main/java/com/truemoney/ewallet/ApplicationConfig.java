package com.truemoney.ewallet;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
/*
 * @ImportResource(value = { "classpath:dynacache-application-context.xml"})
 */
public class ApplicationConfig {

	/*
	 * @Resource(name = "passiveDynaCache") private PassiveDynaCache
	 * passiveDynaCache;
	 * 
	 * @Bean
	 * 
	 * @Primary public PassiveDynaCache passiveDynaCache(){ return
	 * passiveDynaCache; }
	 */

	@Bean(name = "apiRestTemplate")
	@Primary
	public RestTemplate apiRestTemplate() {
		/*
		 * http://stackoverflow.com/questions/13837012/spring-resttemplate-
		 * timeout
		 */
		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setConnectTimeout(SharedConstants.HTTP_CLIENT_CONNECT_TIMEOUT);
		httpRequestFactory.setConnectionRequestTimeout(SharedConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
		httpRequestFactory.setReadTimeout(SharedConstants.HTTP_CLIENT_SOCKET_TIMEOUT);

		RestTemplate restTemplate = new RestTemplate(httpRequestFactory);

		ObjectMapper lax = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
		jacksonConverter.setObjectMapper(lax);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(jacksonConverter);
		return restTemplate;
	}

}
