package vn.mog.payment.dao;

import vn.mog.framework.persistence.dao.UpdatableDAO;
import vn.mog.payment.model.CardList;

public interface ICardListDAO extends UpdatableDAO<CardList, Integer> {

}
