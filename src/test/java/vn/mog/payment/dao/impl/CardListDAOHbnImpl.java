package vn.mog.payment.dao.impl;

import org.springframework.stereotype.Repository;

import vn.mog.framework.persistence.hinernate.dao.IdDAOHbnImpl;
import vn.mog.payment.dao.ICardListDAO;
import vn.mog.payment.model.CardList;

@Repository
public class CardListDAOHbnImpl extends IdDAOHbnImpl<CardList, Integer> implements ICardListDAO {

	@Override
	public Class<CardList> getEntityClass() {
		// TODO Auto-generated method stub
		return CardList.class;
	}

}
