package vn.mog.payment.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import vn.mog.framework.persistence.model.IdEntry;

@Entity
@Table(name = "xxx", uniqueConstraints = @UniqueConstraint(columnNames = { "CUSTOMER_ID", "REQUEST_ID" }))
@AttributeOverride(name = "id", column = @Column(name = "TXN_ID", length = 64))
public class CardList extends IdEntry<Integer> {
	private Integer id;
}
