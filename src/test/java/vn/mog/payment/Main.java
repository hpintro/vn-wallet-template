package vn.mog.payment;

import java.util.UUID;

import com.truemoney.ewallet.common.util.SecurityUtil;

public class Main {
	public static void main(String[] arg) throws Exception {
		String data = "k5Y3GoZb537UIWe6mq+H0bMtvjq6810D+9GaraGsVLogQ9FZaV2L2g==";
		data = SecurityUtil.decrypt(null, data);
		System.out.println("PASS: " + data);
		data = "VBX4dJT2dEBVqkekEfPsAO0ZrJA93K22";
		data = SecurityUtil.decrypt(null, data);
		System.out.println("USER: " + data);

		System.out.println(UUID.randomUUID().toString());
	}

}
